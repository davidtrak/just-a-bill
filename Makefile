SHELL := /bin/bash

# Any dependencies that will trigger a service rebuild on change
FRONT_BUILD_DEPS := docker-compose.yml frontend/Dockerfile frontend/package.json frontend/package-lock.json
BACK_BUILD_DEPS := docker-compose.yml backend/Dockerfile

# Use dummy files '.image' so that make can know to rebuild the 
# service images if any of the dependencies are modified
frontend/.image: $(FRONT_BUILD_DEPS)
	@make front-build

backend/.image: $(BACK_BUILD_DEPS)
	@make back-build


build: $(FRONT_BUILD_DEPS) frontend/.image backend/.image
	@docker-compose rm -fs frontend-test
	-@docker-compose build frontend-test

run-d: .image
	@docker-compose up -d

run-b:
	@docker-compose up --build

run: frontend/.image backend/.image
	@docker-compose up

stop:
	@docker-compose stop

status:
	@docker-compose ps

clean:
	@make front-clean
	@make back-clean
	@docker-compose down
	@docker-compose rm -f

scrub: clean
	@docker system prune -a --volumes

git-clean:
	@git fetch --prune

# This allows you to run any frontend makefile command with 'make front-command'
# Ex. Try 'make front-run' or 'make front-test'
# Alternatively, cd into the frontend directory and run the commands directly
front-%:
	@$(MAKE) --directory frontend/ $(subst front-, , $@)

# This allows you to run any backend makefile command with 'make back-command'
# Ex. Try 'make back-format'
# Alternatively, cd into the backend directory and run the commands directly
back-%:
	@$(MAKE) --directory backend/ $(subst back-, , $@)
