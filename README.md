# Just a bill

## Presentation

For a visual presentation and demo, watch here:

https://youtu.be/oW1Gyg2jShU

## Members

| Name                | UTEID   | GitLab ID   |
| ------------------- | ------- | ----------- |
| David Trakhtengerts | dst726  | @davidtrak  |
| Rodrigo Garcia      | rxg74   | @rigo81401  |
| Connor Popp         | cmp4245 | @P-Connor   |
| Audie Bethea        | ab66273 | @ab66273    |
| Ricky Woodruff      | rgw664  | @woodruffr4 |

## Git SHA

Phase 1: 425d5d1d871748beac2b9c78f06b08a10d7f5b7b

Phase 2: 84d97b157037436dd52fc0b74729ce2dfbc2a46e

Phase 3: aadb8e2c4e5c2f8e8d7b8f69dca699c1ee5376aa

Phase 4: 51bc7823a7015e54d79fa550330d0909cc3c6ea2

## Project Leader

Phase 1: Rodrigo Garcia

Phase 2: David Trakhtengerts

Phase 3: Connor Popp

Phase 4: Ricky Woodruff

## GitLab Pipelines

https://gitlab.com/davidtrak/just-a-bill/-/pipelines

## Website Link

https://justabill.info/

## API Link

https://api.justabill.info/

## Completion Time

### Phase 1

| Name                | Estimated | Actual |
| ------------------- | --------- | ------ |
| David Trakhtengerts | 10        | 12     |
| Rodrigo Garcia      | 10        | 13     |
| Connor Popp         | 10        | 12     |
| Audie Bethea        | 10        | 13     |
| Ricky Woodruff      | 10        | 12     |

### Phase 2

| Name                | Estimated | Actual |
| ------------------- | --------- | ------ |
| David Trakhtengerts | 20        | 25     |
| Rodrigo Garcia      | 15        | 16     |
| Connor Popp         | 15        | 17     |
| Audie Bethea        | 20        | 24     |
| Ricky Woodruff      | 20        | 23     |

### Phase 3

| Name                | Estimated | Actual |
| ------------------- | --------- | ------ |
| David Trakhtengerts | 20        | 25     |
| Rodrigo Garcia      | 20        | 23     |
| Connor Popp         | 20        | 24     |
| Audie Bethea        | 22        | 22     |
| Ricky Woodruff      | 20        | 23     |

### Phase 4

| Name                | Estimated | Actual |
| ------------------- | --------- | ------ |
| David Trakhtengerts | 7         | 8      |
| Rodrigo Garcia      | 8         | 8      |
| Connor Popp         | 9         | 9      |
| Audie Bethea        | 7         | 8      |
| Ricky Woodruff      | 7         | 6      |
