import boto3
from dotenv import load_dotenv
import os
from datetime import date
import requests
from api import db
from api.models import Congressperson, Bill, State
import json
from sqlalchemy import create_engine
import sys
import base64

load_dotenv()

access_key = os.getenv("S3_ACCESS_KEY")
secret_key = os.getenv("S3_SECRET_KEY")

s3 = boto3.resource(
    service_name="s3", aws_access_key_id=access_key, aws_secret_access_key=secret_key
)

bucket = s3.Bucket("justabill-pdfs")

headers = {"X-API-Key": os.getenv("BILLS_API_KEY")}

bills = db.session.query(Bill).all()
for bill in bills:
    bill_id = bill.id
    bill_url = bill.pdf_url
    res = requests.get(bill_url, headers)
    print(res.status_code)
    pdf = base64.b64encode(res.content)
    if sys.getsizeof(pdf) != 60445:
        with open("pdfs/{}.pdf".format(bill_id), "wb") as f:
            f.write(pdf)
            bucket.upload_file(Filename=f"pdfs/{bill_id}.pdf", Key=f"{bill_id}.pdf")


db.session.commit()
