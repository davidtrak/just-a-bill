from datetime import date
from dotenv import load_dotenv
import os, requests
from api import db
from api.models import Congressperson, Bill, State
import json
from sqlalchemy import create_engine


load_dotenv()


def update_congresspeople():
    url = "https://api.propublica.org/congress/v1/117/senate/members.json"
    headers = {"X-API-Key": os.getenv("X-API-Key")}
    res = requests.get(url=url, headers=headers)
    data = res.json()["results"][0]["members"]

    congress_dict = {}

    for person in data:
        middle_name = (
            " " + person["middle_name"] + " " if person["middle_name"] else " "
        )
        full_name = person["first_name"] + middle_name + person["last_name"]
        congress_dict[full_name] = {
            "title": person["title"],
            "short_title": person["short_title"],
        }

    url = "https://api.propublica.org/congress/v1/117/house/members.json"
    headers = {"X-API-Key": os.getenv("X-API-Key")}
    res = requests.get(url=url, headers=headers)
    data = res.json()["results"][0]["members"]

    for person in data:
        middle_name = (
            " " + person["middle_name"] + " " if person["middle_name"] else " "
        )
        full_name = person["first_name"] + middle_name + person["last_name"]
        congress_dict[full_name] = {
            "title": person["title"],
            "short_title": person["short_title"],
        }

    congresspeople = db.session.query(Congressperson).all()
    for person in congresspeople:
        element = congress_dict[person.full_name]

        if element is not None:
            person.title = element["title"]
            person.short_title = element["short_title"]
            print("successfully updated {}".format(person.full_name))

    db.session.commit()


# Add congresspeople to db
def add_congresspeople():
    # Get json list of members
    data = pull_congresspeople()

    # Add each one with unique id : i
    for i, person in enumerate(data):
        add_congressperson(person, i)

    db.session.commit()


# Pull congresspeople from api (or files)
# Currently assumes data/senate.json and data/house.json exist
# Combines the memembers and returns that json
def pull_congresspeople():
    # Does not work atm.
    # url = "https://api.propublica.org/congress/v1/117/**house or senate**}/members.json"
    # header = {"X-API-Key": os.getenv("X-API-Key")}
    # res = requests.get(
    #     url=url,
    #     headers=header
    # )
    # res = res.json()

    f = open("./data/senate.json")
    f2 = open("./data/house.json")

    res = json.load(f)
    res2 = json.load(f2)

    res = res["results"][0]["members"]
    res2 = res2["results"][0]["members"]

    return res + res2


# Given one entry and id, add to db
def add_congressperson(person, i):
    middle_name = " " + person["middle_name"] + " " if person["middle_name"] else " "
    full_name = person["first_name"] + middle_name + person["last_name"]
    entry = dict()
    entry["id"] = i
    entry["first_name"] = person["first_name"]
    entry["middle_name"] = person["middle_name"]
    entry["last_name"] = person["last_name"]
    entry["full_name"] = full_name
    entry["age"] = calculate_age(person["date_of_birth"])
    entry["gender"] = person["gender"]
    entry["party"] = person["party"]
    entry["seniority"] = person["seniority"]
    entry["in_office"] = person["in_office"]
    entry["next_election"] = person["next_election"]
    entry["terms_served"] = get_terms_served(person)
    entry["district"] = person["district"] if "district" in person else None
    entry["twitter_account"] = person["twitter_account"]
    entry["facebook_account"] = person["facebook_account"]
    entry["youtube_account"] = person["youtube_account"]

    person_entry = Congressperson(**entry)

    # Link person to state
    link_person_to_state(person["state"], person_entry)

    db.session.add(person_entry)


# Add congressperson to list list of reps of state
def link_person_to_state(abv, person_entry):
    state = db.session.query(State).filter_by(abbreviation=abv).first()
    if state:
        state.representatives.append(person_entry)


# Return int of number of roles a congressperson had
def get_terms_served(person):
    url = f"https://api.propublica.org/congress/v1/members/{person['id']}.json"
    header = {"X-API-Key": os.getenv("X-API-Key")}
    res = requests.get(url=url, headers=header)
    res = res.json()

    return len(res["results"][0]["roles"])


# Return age from DOB
def calculate_age(born):
    year, month, day = [int(x) for x in born.split("-")]
    today = date.today()
    return today.year - year - ((today.month, today.day) < (month, day))


# Pull bills from api and save them into data/bills
# API returns with offset of 20
def pull_bills():

    # Increment of 20
    NUM_BILLS = 100

    with open("./data/bills.json", mode="w") as f:
        json.dump({"bills": []}, f)

    offset = 0

    while offset < NUM_BILLS:
        url = (
            "https://api.propublica.org/congress/v1/117/both/bills/introduced.json?offset="
            + str(offset)
        )
        header = {"X-API-Key": os.getenv("X-API-Key")}
        res = requests.get(url=url, headers=header)

        data = res.json()
        data = data["results"][0]["bills"]

        # Read in current bills
        with open("./data/bills.json", mode="r") as f:
            load = json.load(f)

        # Add new bills
        for i in range(20):
            load["bills"].append(data[i])

        # Write back to file
        with open("./data/bills.json", mode="w") as f:
            json.dump(load, f)

        offset += 20


# Add bills from data/bills.json
def add_bills():

    # Pull bills from api
    # pull_bills()

    f = open("./data/bills.json")

    res = json.load(f)

    res = res["bills"]

    # Add each bill with a unique id : i
    for i, bill in enumerate(res):
        add_bill(bill, i)

    db.session.commit()


# Add a bill to the db with a unique id
def add_bill(bill, i):
    entry = dict()
    entry["id"] = i
    entry["number"] = bill["number"]
    entry["title"] = bill["title"]
    entry["short_title"] = bill["short_title"]
    entry["description"] = bill["summary"]
    entry["enacted"] = bill["enacted"]
    entry["active"] = bill["active"]
    entry["pdf_url"] = get_pdf_url(bill)
    entry["introduced_date"] = bill["introduced_date"]
    entry["sponsor_name"] = bill["sponsor_name"]
    entry["sponsor_party"] = bill["sponsor_party"]
    entry["senate_passage"] = bill["senate_passage"]
    entry["house_passage"] = bill["house_passage"]
    entry["republican_vote_tally"] = None
    entry["democrat_vote_tally"] = None
    entry["cosponsors"] = bill["cosponsors"]
    entry["committees"] = bill["committees"]
    entry["primary_subject"] = bill["primary_subject"]

    bill_entry = Bill(**entry)

    # Link bill to state and congresspeople
    link_bill_to_state(bill["sponsor_state"], bill_entry)
    link_bill_to_congresspeople(bill["sponsor_name"], bill_entry)

    db.session.add(bill_entry)


# Generate pdf url
def get_pdf_url(bill):
    congress = "117"
    bill_str = congress + bill["bill_slug"]
    return f"https://www.govinfo.gov/content/pkg/BILLS-{bill_str}ih/pdf/BILLS-{bill_str}ih.pdf"


# Add bill to state's bill list
def link_bill_to_state(state_abv, bill_entry):
    state = db.session.query(State).filter_by(abbreviation=state_abv).first()
    if state:
        state.bills_proposed.append(bill_entry)


# Add bill to congresspersons bill list
def link_bill_to_congresspeople(name, bill_entry):
    person = db.session.query(Congressperson).filter_by(full_name=name).first()
    if person:
        person.bills_proposed.append(bill_entry)


# Add all states to the db
def add_states():

    # Pull states from api
    data = pull_states()

    # Add each one with unique id : i
    for i, state in enumerate(data):
        add_state(state, i)

    db.session.commit()


# Pull all the states from the api
def pull_states():
    url = "https://parseapi.back4app.com/classes/Usstatesdataset_States?limit=50&order=name&excludeKeys=landAreaSquareKilometers,landAreaSquareMiles,totalAreaSquareKilometers,waterAreaSquareKilometers,waterAreaSquareMiles"
    headers = {
        "X-Parse-Application-Id": os.getenv("STATES_PARSE_APP_ID"),
        "X-Parse-REST-API-Key": os.getenv("STATES_PARSE_REST_API_KEY"),
    }
    res = requests.get(url=url, headers=headers)

    data = res.json()
    return data


# Add a state to the db
def add_state(i, state):
    entry = dict()
    entry["id"] = i
    entry["name"] = state["name"]
    entry["flag"] = state["flag"]
    entry["abbreviation"] = state["postalAbreviation"]
    entry["capital"] = state["capital"]
    entry["largest_city"] = state["largestCity"]
    entry["established"] = state["established"]
    entry["population"] = state["population"]
    entry["size"] = state["totalAreaSquareMiles"]
    entry["num_congressmen"] = state["numberRepresentatives"]

    db.session.add(State(**entry))


# Returns the state's name for this geo, None if it is invalid
def pull_state_name(geo):
    url = "https://datausa.io/api/data?measure=Poverty%20Population&year=latest&Geography=04000US{}".format(
        geo
    )

    res = requests.get(url)
    data = res.json()["data"][0]
    if "Geography" not in data:
        return None
    return data["Geography"]


# Takes in a datausa url and the number of people in poverty in this state. Returns None if not a valid state ID
def pull_state_poverty(geo):
    url = "https://datausa.io/api/data?measure=Poverty%20Population&year=latest&Geography=04000US{}".format(
        geo
    )

    res = requests.get(url)
    data = res.json()["data"][0]
    return data["Poverty Population"]


# Takes in a datausa url and returns the 5 most common ethnic groups, in order
def pull_state_ethnic(geo):
    url = "https://datausa.io/api/data?drilldowns=Race,Ethnicity&measures=Hispanic%20Population&year=latest&Geography=04000US{}".format(
        geo
    )

    res = requests.get(url)
    data = res.json()["data"]

    ethnic_dict = {}

    for element in data:
        race = element["Race"]
        pop = element["Hispanic Population"]
        eth = element["Ethnicity"]

        if eth == "Hispanic or Latino":
            race = eth

        if race in ethnic_dict:
            ethnic_dict[race] += pop
        else:
            ethnic_dict[race] = pop

    sorted_ed = sorted(ethnic_dict.items(), key=lambda x: x[1], reverse=True)[:5]

    sb = ""
    for pair in sorted_ed:
        sb += str(pair[0]) + ", "
    sb = sb[: len(sb) - 2]

    return sb


# Takes in a datausa url and returns median household income for the state
def pull_state_income(geo):
    url = "https://datausa.io/api/data?measure=Household%20Income%20by%20Race&year=latest&Geography=04000US{}".format(
        geo
    )

    res = requests.get(url)
    data = res.json()["data"][0]
    return data["Household Income by Race"]


# Add information about states that was not included in the back4app api
def pull_state_demographics():
    demo_dict = {}

    for i in range(1, 57):
        geo = str(i)
        if len(geo) == 1:
            geo = "0" + geo

        name = pull_state_name(geo)

        if name != None and name != "District of Columbia":
            in_poverty = pull_state_poverty(geo)
            ethnic_groups = pull_state_ethnic(geo)
            median_income = pull_state_income(geo)
            demo_dict[name] = {
                "in_poverty": in_poverty,
                "ethnic_groups": ethnic_groups,
                "median_household_income": median_income,
            }

    json_string = json.dumps(demo_dict)
    with open("state_demographics.json", "w") as outfile:
        outfile.write(json_string)


# Use python beautifulsoup to scrape a description and party afil of each state
def scrape_state_data():
    # Hard code state affiliations because its hard to scrape
    rep_states = [
        "Alabama",
        "Alaska",
        "Arizona",
        "Arkansas",
        "Florida",
        "Idaho",
        "Indiana",
        "Kansas",
        "Mississippi",
        "Missouri",
        "Montana",
        "Nebraska",
        "North Dakota",
        "Ohio",
        "Oklahoma",
        "South Carolina",
        "South Dakota",
        "Tennessee",
        "Texas",
        "Utah",
        "West Virginia",
        "Wyoming",
    ]

    with open("state_demographics.json") as json_file:
        data = json.load(json_file)

    for name in data.keys():
        oriname = name
        name = name.split(" ")
        name = "-".join(name)
        # scrape description
        url = "https://datausa.io/profile/geo/{}".format(name.lower())
        page = requests.get(url)
        soup = BeautifulSoup(page.content, "html.parser")
        line = soup.find("div", {"class": "section-description"}).getText()
        line = line.split("About the photo:")[0]
        data[oriname]["description"] = line

        # derive affiliation
        if name == "Wyoming":
            data[oriname]["party_affiliation"] = "Even"
        elif name in rep_states:
            data[oriname]["party_affiliation"] = "Republican"
        else:
            data[oriname]["party_affiliation"] = "Democratic"

        assert data[oriname]["description"] != None
        assert data[oriname]["party_affiliation"] != None

    json_string = json.dumps(data)
    with open("state_demographics.json", "w") as outfile:
        outfile.write(json_string)


# Update the columns of the states table that are contained in the scraped json file.
def update_states_from_json():
    with open("state_demographics.json") as json_file:
        data = json.load(json_file)

    states = db.session.query(State).all()
    for state in states:
        # for state in data.keys():
        element = data[state.name]

        state.description = element["description"]
        state.in_poverty = int(element["in_poverty"])
        state.ethnic_groups = element["ethnic_groups"]
        state.median_household_income = int(element["median_household_income"])
        # state.party_affiliation = element['party_affiliation']

    db.session.commit()


# Reset entire db
def reset_db():
    pass
    # db.drop_all()
    # db.create_all()


# Example query to update a column
def update_table():
    people = db.session.query(Congressperson).all()
    for person in people:
        middle_name = " " + person.middle_name + " " if person.middle_name else " "
        person.full_name = person.first_name + middle_name + person.last_name

    db.session.commit()


# Reset a specific table
def reset_table():
    engine = create_engine(os.getenv("AWS_CONNECTION_KEY"))
    # Bill.__table__.drop(engine)
    # Bill.__table__.create(engine)
    # Congressperson.__table__.drop(engine)
    # engine.execute("DROP TABLE %s CASCADE;" % "congresspeople")
    # Congressperson.__table__.create(engine)


if __name__ == "__main__":
    # reset_db()
    # reset_table()
    # update_table()
    # add_congresspeople()
    # add_bills()
    # add_states()
    # update_congresspeople()
    pass
