import requests
from unittest import main, TestCase

HOSTNAME = "https://api.justabill.info"


class UnitTests(TestCase):

    # main page of API loads without error
    def test_0(self):
        result = requests.get(HOSTNAME)
        self.assertEqual(result.status_code, 200)

    # Test getting state by ID
    def test_1(self):
        test_schema = {
            "abbreviation": "AL",
            "capital": "Montgomery",
            "description": "In 2019, Alabama had a population of 4.9M people with a median age of 39.4 and a median household income of $51,734. Between 2018 and 2019 the population of Alabama grew from 4.89M to 4.9M, a 0.313% increase and its median household income grew from $49,861 to $51,734, a 3.76% increase.The 5 largest ethnic groups in Alabama are  White (Non-Hispanic) (65.1%), Black or African American (Non-Hispanic) (26.8%), White (Hispanic) (2.72%), Two+ (Non-Hispanic) (1.68%), and Asian (Non-Hispanic) (1.33%). 5.53% of the households in Alabama speak a non-English language at home as their primary language.98% of the residents in Alabama are U.S. citizens.The largest universities in Alabama are The University of Alabama (9,728 degrees awarded in 2019), Columbia Southern University (7,635 degrees), and Auburn University (7,521 degrees).In 2019, the median property value in Alabama was $154,000, and the homeownership rate was 68.8%. Most people in Alabama drove alone to work, and the average commute time was 24.6 minutes. The average car ownership in Alabama was 2 cars per household.Alabama borders Florida, Georgia, Mississippi, and Tennessee.",
            "established": "1819-12-14",
            "ethnic_groups": "White Alone; Black or African American Alone; Hispanic or Latino; Two or More Races; Asian Alone",
            "flag": "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Alabama.svg/23px-Flag_of_Alabama.svg.png",
            "id": 0,
            "in_poverty": 4754288,
            "largest_city": "Birmingham",
            "median_household_income": 51734,
            "name": "Alabama",
            "num_congressmen": 7,
            "party_affiliation": "Republican",
            "population": 4874747,
            "size": 52420,
        }
        result = requests.get("{}/states/0".format(HOSTNAME))
        self.assertEqual(result.status_code, 200)
        data = result.json()
        for key in test_schema.keys():
            self.assertEqual(test_schema[key], data[key])

    # Test getting bills by ID
    def test_2(self):
        test_schema = {
            "active": False,
            "committees": "House Transportation and Infrastructure Committee",
            "congressperson_id": 399,
            "cosponsors": 1,
            "id": 0,
            "introduced_date": "2022-04-07",
            "bill": "H.R.7476",
            "pdf_url": "https://www.govinfo.gov/content/pkg/BILLS-117hr7476ih/pdf/BILLS-117hr7476ih.pdf",
            "primary_subject": "",
            "short_title": "To amend the Disaster Recovery Reform Act of 2018 to require the President to automatically waive certain critical document fees for individuals and households affected by major disasters for which assistance is provided under the Individuals and Househol",
            "sponsor": "Joe Neguse",
            "sponsor_party": "D",
            "state_id": 5,
            "title": "To amend the Disaster Recovery Reform Act of 2018 to require the President to automatically waive certain critical document fees for individuals and households affected by major disasters for which assistance is provided under the Individuals and Households Program.",
        }
        result = requests.get("{}/bills/0".format(HOSTNAME))
        self.assertEqual(result.status_code, 200)
        data = result.json()
        for key in test_schema.keys():
            self.assertEqual(test_schema[key], data[key])

    # Test getting congressperson by ID
    def test_3(self):
        test_schema = {
            "age": 60,
            "facebook_account": "senatortammybaldwin",
            "first_name": "Tammy",
            "full_name": "Tammy Baldwin",
            "gender": "F",
            "id": 0,
            "in_office": True,
            "last_name": "Baldwin",
            "next_election": "2024",
            "party": "D",
            "seniority": 9,
            "state_id": 48,
            "terms_served": 12,
            "twitter_account": "SenatorBaldwin",
            "youtube_account": "witammybaldwin",
        }
        result = requests.get("{}/congresspeople/0".format(HOSTNAME))
        self.assertEqual(result.status_code, 200)
        data = result.json()
        for key in test_schema.keys():
            self.assertEqual(test_schema[key], data[key])

    # Test getting state by page paginates the correct amount
    def test_4(self):
        result = requests.get("{}/states".format(HOSTNAME))
        self.assertEqual(result.status_code, 200)
        data = result.json()["page"]
        self.assertEqual(len(data), 20)

    # Test getting state by page paginates the correct amount
    def test_5(self):
        result = requests.get("{}/bills".format(HOSTNAME))
        self.assertEqual(result.status_code, 200)
        data = result.json()["page"]
        self.assertEqual(len(data), 20)

    # Test getting state by page paginates the correct amount
    def test_6(self):
        result = requests.get("{}/congresspeople".format(HOSTNAME))
        self.assertEqual(result.status_code, 200)
        data = result.json()["page"]
        self.assertEqual(len(data), 20)


if __name__ == "__main__":
    main()
