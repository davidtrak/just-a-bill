from flask import request
from api.queries import congresspeople, bills, states, pdfs
from api import app
from api.models import *


@app.route("/")
def main():
    return "Hello World!"


# Bills


@app.route("/bills", methods=["get"])
def get_bills():
    options = request.args.to_dict(flat=False)

    return bills.get_bills(options)


@app.route("/bills/<id>", methods=["get"])
def get_bill_by_id(id):

    options = {}
    options["id"] = id

    return bills.get_bill_by_id(options)


# Congresspeople


@app.route("/congresspeople", methods=["get"])
def get_congresspeople():
    options = request.args.to_dict(flat=False)

    return congresspeople.get_congresspeople(options)


@app.route("/congresspeople/<id>", methods=["get"])
def get_congressperson_by_id(id):

    options = {}
    options["id"] = id

    return congresspeople.get_congressperson_by_id(options)


# States


@app.route("/states", methods=["get"])
def get_states():
    options = request.args.to_dict(flat=False)

    return states.get_states(options)


@app.route("/states/<id>", methods=["get"])
def get_state_by_id(id):
    options = {}
    options["id"] = id

    return states.get_state_by_id(options)


# PDFs


@app.route("/pdf/<id>", methods=["get"])
def get_pdf(id):
    return pdfs.get_pdf_by_id(id)
