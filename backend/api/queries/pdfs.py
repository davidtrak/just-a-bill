import boto3
import os


def get_pdf_by_id(id):
    filename = f"{id}.pdf"

    access_key = os.getenv("S3_ACCESS_KEY")
    secret_key = os.getenv("S3_SECRET_KEY")

    s3 = boto3.client(
        service_name="s3",
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
    )

    obj = s3.get_object(Bucket="justabill-new-pdfs", Key=filename)
    return obj["Body"].read()
