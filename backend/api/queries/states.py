from api import db
from api.models import State
from api.schemas import StateSchema
from sqlalchemy import or_, case
from .bills import populate_filters


def get_states(options):
    """
    :param options: A dictionary containing all the paramters for the Operations
        options["page"]: Page number

    """

    states = db.session.query(State)

    # Filter
    states = filter_states(states, options)

    # Search
    states = search_states(states, options)

    # Sort
    states = sort_states(states, options)

    # Paginate
    page_of_states = paginate(states, options)

    page_of_states = StateSchema(exclude=("bills_proposed", "representatives")).dump(
        page_of_states.items, many=True
    )

    total_results = states.count()

    return {"total_results": total_results, "page": page_of_states}, 200


def get_state_by_id(options):
    """
    :param options: A dictionary containing all the paramters for the Operations
        options["id"]: ID of the state

    """

    state = db.session.query(State).filter_by(id=options["id"])
    state = StateSchema().dump(state, many=True)[0]

    return state, 200


# Filter state query by attributes
# Allow filtering by list per attribute
def filter_states(states, options):
    filterable_attr = [
        "location",
        "party_affiliation",
    ]

    for attr in filterable_attr:
        if attr in options:
            states = states.filter(State.__dict__[attr].in_(options[attr]))

    # Special cases
    if "size" in options:
        size_ranges = {
            "small": [0, 25000],
            "medium": [25000, 70000],
            "large": [70000, 999999],
        }
        size = options["size"][0]
        if size in size_ranges:
            states = states.filter(
                State.size.between(size_ranges[size][0], size_ranges[size][1])
            )

    if "population" in options:
        pop_ranges = {
            "small": [0, 1000000],
            "medium": [1000000, 10000000],
            "large": [10000000, 100000000],
        }
        pop = options["population"][0]
        if pop in pop_ranges:
            states = states.filter(
                State.population.between(pop_ranges[pop][0], pop_ranges[pop][1])
            )

    return states


# Sort state query by an attribute, descending if specified
def sort_states(states, options):
    sortable_attr = [
        "size",
        "population",
        "name",
        "num_congressmen",
        "num_proposed_bills",
        "median_household_income",
        "established",
        "in_poverty",
    ]

    if "sort" in options:
        attr = None
        desc = False
        split = options["sort"][0].split("-")
        if len(split) == 2:
            attr = split[1]
            desc = True
        else:
            attr = split[0]

        if attr in sortable_attr:
            if desc:
                states = states.order_by(State.__dict__[attr].desc())
            else:
                states = states.order_by(State.__dict__[attr])

    return states


# Paginate state query, defaulting to page 1 and 20 results per page
def paginate(states, options):
    page = 1
    per_page = 20

    if "page" in options:
        page = int(options["page"][0])

    if "per_page" in options:
        per_page = int(options["per_page"][0])

    return states.paginate(page, per_page, False)


# Search state query by string using or operator
def search_states(states, options):
    if "search" not in options:
        return states

    # Attributes in State model
    state_searchable_attr = [
        "name",
        "abbreviation",
        "largest_city",
        "ethnic_groups",
        "capital",
        "party_affiliation",
    ]

    # Populate filters by model type
    filters = []
    populate_filters(options, state_searchable_attr, filters, State)

    # Filter by or of all search attributes
    states = states.filter(or_(*filters))

    # Order by most matched options
    if len(options["search"][0].split(" ")) > 1:
        states = states.order_by(None).order_by(
            (sum([case((f, 1), else_=0) for f in filters])).desc()
        )

    return states
