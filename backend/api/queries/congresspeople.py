from api import db
from api.models import Congressperson, State
from api.schemas import CongresspersonSchema
from sqlalchemy import or_, case
from .bills import populate_filters


def get_congresspeople(options):
    """
    :param options: A dictionary containing all the paramters for the Operations
        options["page"]: Page number

    """

    people = db.session.query(Congressperson)

    # Filter
    people = filter_people(people, options)

    # Search
    people = search_people(people, options)

    # Sort
    people = sort_people(people, options)

    # Paginate
    page_of_people = paginate(people, options)

    page_of_people = CongresspersonSchema().dump(page_of_people.items, many=True)

    total_results = people.count()

    return {"total_results": total_results, "page": page_of_people}, 200


def get_congressperson_by_id(options):
    """
    :param options: A dictionary containing all the paramters for the Operations
        options["id"]: ID of the congressperson

    """

    congressperson = db.session.query(Congressperson).filter_by(id=options["id"])
    congressperson = CongresspersonSchema().dump(congressperson, many=True)[0]

    return congressperson, 200


# Filter people query by attributes
# Allow filtering by list per attribute
def filter_people(people, options):
    filterable_attr = [
        "gender",
        "party",
        "short_title",
        "state",
        "leadership_role",
        "state_id",
    ]

    for attr in filterable_attr:
        if attr in options:
            people = people.filter(Congressperson.__dict__[attr].in_(options[attr]))

    return people


# Sort people query by an attribute, descending if specified
def sort_people(people, options):
    sortable_attr = [
        "full_name",
        "age",
        "next_election",
        "seniority",
        "terms_served",
        "bills_sponsored",
    ]

    if "sort" in options:
        attr = None
        desc = False
        split = options["sort"][0].split("-")
        if len(split) == 2:
            attr = split[1]
            desc = True
        else:
            attr = split[0]

        if attr in sortable_attr:
            if desc:
                people = people.order_by(Congressperson.__dict__[attr].desc())
            else:
                people = people.order_by(Congressperson.__dict__[attr])

    return people


# Paginate people query, defaulting to page 1 and 20 results per page
def paginate(people, options):
    page = 1
    per_page = 20

    if "page" in options:
        page = int(options["page"][0])

    if "per_page" in options:
        per_page = int(options["per_page"][0])

    return people.paginate(page, per_page, False)


# Filter people query by search param in options
def search_people(people, options):
    if "search" not in options:
        return people

    # Attributes in Congresspeople Model
    person_searchable_attr = [
        "full_name",
        "district",
        "party",
        "title",
        "leadership_role",
    ]

    # Attributes in State Model
    state_searchable_attr = ["name"]

    # Join State table to allow to search by state attributes
    people = people.join(State)

    # Populate filters by model type
    filters = []
    populate_filters(options, person_searchable_attr, filters, Congressperson)
    populate_filters(options, state_searchable_attr, filters, State)

    # Filter by or of all search attributes
    people = people.filter(or_(*filters))

    # Order by most matched options
    if len(options["search"][0].split(" ")) > 1:
        people = people.order_by(None).order_by(
            (sum([case((f, 1), else_=0) for f in filters])).desc()
        )

    return people
