from api import db
from api.models import Bill, Congressperson, State
from api.schemas import BillSchema
from sqlalchemy import or_, case, func


def get_bills(options):
    """
    :param options: A dictionary containing all the paramters for the Operations
        options["page"]: Page number

    """

    bills = db.session.query(Bill)

    # Filter
    bills = filter_bills(bills, options)

    # Search
    bills = search_bills(bills, options)

    # Sort
    bills = sort_bills(bills, options)

    # Paginate
    page_of_bills = paginate(bills, options)

    page_of_bills = BillSchema().dump(page_of_bills.items, many=True)

    total_results = bills.count()

    return {"total_results": total_results, "page": page_of_bills}, 200


def get_bill_by_id(options):
    """
    :param options: A dictionary containing all the paramters for the Operations
        options["id"]: ID of the bill

    """

    bill = db.session.query(Bill).filter_by(id=options["id"])
    bill = BillSchema().dump(bill, many=True)[0]

    return bill, 200


# Filter bill query by attributes
# Allow filtering by list per attribute
def filter_bills(bills, options):
    filterable_attr = [
        "active",
        "sponsor_party",
        "sponsor_title",
        "sponsor_state",
        "state_id",
        "congressperson_id",
    ]

    for attr in filterable_attr:
        if attr in options:
            bills = bills.filter(Bill.__dict__[attr].in_(options[attr]))

    return bills


# Sort bill query by an attribute, descending if specified
def sort_bills(bills, options):
    sortable_attr = [
        "short_title",
        "bill",
        "introduced_date",
        "latest_major_action_date",
        "cosponsors",
    ]

    if "sort" in options:
        attr = None
        desc = False
        split = options["sort"][0].split("-")
        if len(split) == 2:
            attr = split[1]
            desc = True
        else:
            attr = split[0]

        if attr in sortable_attr:
            if desc:
                bills = bills.order_by(Bill.__dict__[attr].desc())
            else:
                bills = bills.order_by(Bill.__dict__[attr])

    return bills


# Paginate bill query, default to page 1 and 20 results per page
def paginate(bills, options):
    page = 1
    per_page = 20

    if "page" in options:
        page = int(options["page"][0])

    if "per_page" in options:
        per_page = int(options["per_page"][0])

    return bills.paginate(page, per_page, False)


# Filter bill query by search param in options
def search_bills(bills, options):
    if "search" not in options:
        return bills

    # Attributes in Bill Model
    bill_searchable_attr = ["title", "short_title", "bill", "bill_slug", "committees"]

    # Attributes in Congressperson Model
    person_searchable_attr = ["full_name"]

    # Attributes in State Model
    state_searchable_attr = ["name"]

    # Join tables to allow to search diff model attributes
    bills = bills.join(Congressperson).join(State)

    # Populate filters by model type
    filters = []
    populate_filters(options, bill_searchable_attr, filters, Bill)
    populate_filters(options, person_searchable_attr, filters, Congressperson)
    populate_filters(options, state_searchable_attr, filters, State)

    # Filter by or of all search attributes
    bills = bills.filter(or_(*filters))

    # Order by most matched options
    if len(options["search"][0].split(" ")) > 1:
        bills = bills.order_by(None).order_by(
            (sum([case((f, 1), else_=0) for f in filters])).desc()
        )

    return bills


# Fill filters list given a model, options, attributes
def populate_filters(options, attributes, filters, model):
    for attr in attributes:
        for term in options["search"][0].split(" "):
            filters.append(model.__dict__[attr].ilike("%" + term + "%"))
