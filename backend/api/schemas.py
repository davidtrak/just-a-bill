from api.models import State, Congressperson, Bill
from api import ma


class BillSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Bill
        include_fk = True


class CongresspersonSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Congressperson
        include_fk = True

    state_name = ma.Function(
        lambda obj: obj.represents.name if obj.represents else None
    )


class StateSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = State
        include_fk = True

    bills_proposed = ma.Nested(BillSchema, many=True)
    representatives = ma.Nested(CongresspersonSchema, many=True)
