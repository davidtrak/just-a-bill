from datetime import date
from dotenv import load_dotenv
import os, requests
from api import db
from api.models import Congressperson, Bill, State
import json
from sqlalchemy import create_engine

load_dotenv(override=True)

headers = {"X-API-Key": os.getenv("CONGRESS_API_KEY")}

bulk_keys = [
    "first_name",
    "middle_name",
    "last_name",
    "suffix",
    "date_of_birth",
    "gender",
    "leadership_role",
    "party",
    "twitter_account",
    "facebook_account",
    "youtube_account",
    "url",
    "seniority",
    "state",
    "in_office",
    "next_election",
    "total_votes",
    "missed_votes",
    "office",
    "phone",
    "fax",
    "senate_class",
    "state_rank",
    "missed_votes_pct",
    "votes_with_party_pct",
    "votes_against_party_pct",
    "title",
    "short_title",
    "district",
    "contact_form",
    "last_updated",
]

specific_keys = ["most_recent_vote", "member_id"]


def build_fullname(person):
    middle_name = " " + person["middle_name"] + " " if person["middle_name"] else " "
    return person["first_name"] + middle_name + person["last_name"]


# Return age from DOB
def calculate_age(born):
    year, month, day = [int(x) for x in born.split("-")]
    today = date.today()
    return today.year - year - ((today.month, today.day) < (month, day))


def load_entry(member):
    member_entry = {}
    member_keys = member.keys()

    for key in bulk_keys:
        if key in member_keys:
            member_entry[key] = member[key]
        else:
            member_entry[key] = None

    api_uri = member["api_uri"]
    res = requests.get(url=api_uri, headers=headers)

    member = res.json()["results"][0]
    for key in specific_keys:
        member_entry[key] = member[key]
    member_entry[
        "headshot_url"
    ] = "https://theunitedstates.io/images/congress/450x550/{}.jpg".format(
        member_entry["member_id"]
    )

    role = member["roles"][0]
    member_entry["bills_sponsored"] = role["bills_sponsored"]
    member_entry["bills_cosponsored"] = role["bills_cosponsored"]
    member_entry["terms_served"] = len(member["roles"])
    com = []
    for committee in role["committees"]:
        com.append(committee["name"])

    member_entry["committees"] = "; ".join(com)
    member_entry["num_committees"] = len(com)
    member_entry["age"] = calculate_age(member["date_of_birth"])
    member_entry["full_name"] = build_fullname(member)

    return member_entry


def pull_senate():
    url = "https://api.propublica.org/congress/v1/117/senate/members.json"
    res = requests.get(url=url, headers=headers)
    data = res.json()["results"][0]["members"]

    congress_list = []

    for member in data:
        full_name = build_fullname(member)
        entry = load_entry(member)
        entry["chamber"] = "Senate"
        entry["full_name"] = full_name
        congress_list.append(entry)

    return congress_list


def pull_house():
    url = "https://api.propublica.org/congress/v1/117/house/members.json"
    res = requests.get(url=url, headers=headers)
    data = res.json()["results"][0]["members"]

    congress_list = []

    for member in data:
        full_name = build_fullname(member)
        entry = load_entry(member)
        entry["chamber"] = "House"
        entry["full_name"] = full_name
        if entry["title"] == "Representative":
            congress_list.append(entry)

    return congress_list


# Given one entry and id, add to db
def add_congressperson(entry, i):
    entry["id"] = i
    person_entry = Congressperson(**entry)

    # Link person to state
    link_person_to_state(entry["state"], person_entry)
    db.session.add(person_entry)


# Add congressperson to list list of reps of state
def link_person_to_state(abv, person_entry):
    state = db.session.query(State).filter_by(abbreviation=abv).first()
    if state:
        state.representatives.append(person_entry)


# Reset a specific table
def reset_table():
    engine = create_engine(os.getenv("TEST_AWS_CONNECTION_KEY"))
    Congressperson.__table__.drop(engine)
    # engine.execute("DROP TABLE %s CASCADE;" % "congresspeople")
    Congressperson.__table__.create(engine)


def main():
    reset_table()
    senate_list = pull_senate()
    house_list = pull_house()
    congress_list = senate_list + house_list

    # Add each one with unique id : i
    for i, person in enumerate(congress_list):
        add_congressperson(person, i)
        print("added cp {}".format(i))

    db.session.commit()


if __name__ == "__main__":
    main()
