from datetime import date
from dotenv import load_dotenv
import os, requests
from api import db
from api.models import Congressperson, Bill, State
import json
from sqlalchemy import create_engine
import sys
import base64
from patch_states import pull_state_poverty, pull_state_name

load_dotenv(override=True)

headers = {"X-API-Key": os.getenv("CONGRESS_API_KEY")}

"""
States:
    - in_poverty: sorting by in_poverty gives literally the same results as sorting
        by population. Make in_poverty a percentage so this is better 
        (also I think there may be something wrong with it because those 
        numbers look absurdly high)
    - party_affiliation: change "Democratic" to "Democrat" for my sanity pls, 
        also maybe there could be more even states but idk how we're getting 
        this info (also fix wyoming/wisconsin)
    - location: fill out with census regions (Northeast, Midwest, South, and West)
    - num_proposed_bills: fill out (total number)
    - established: when sorting by established date, it
        does it in alphabetical order (so literally just by the month). 
        We can either remove it from the sortable attributes, reformat the 
        value so that it sorts nicely ("Jun 20, 1863" to "1863-06-20"), or 
        change the sort logic to use dates somehow
"""

# Add information about states that was not included in the back4app api
def repair_poverty():
    for i in range(1, 57):
        geo = str(i)
        if len(geo) == 1:
            geo = "0" + geo

        name = pull_state_name(geo)

        if name != None and name != "District of Columbia":
            state = db.session.query(State).filter_by(name=name).first()
            in_poverty = pull_state_poverty(geo)
            state.in_poverty = in_poverty


def fix_poverty(state):
    raw_not_in_poverty = state.in_poverty
    raw_population = state.population
    raw_in_poverty = raw_population - raw_not_in_poverty

    poverty_percentage = raw_in_poverty / raw_population

    # state.in_poverty_percentage = poverty_percentage


def fix_afil(state):
    name = state.name
    cur_party = state.party_affiliation

    rep_patch = [
        "Wyoming",
        "West Virginia",
        "North Dakota",
        "South Carolina",
        "South Dakota",
    ]
    party = ""

    if name == "Wisconsin":
        party = "Even"
    elif name in rep_patch:
        party = "Republican"
    elif cur_party == "Democratic":
        party = "Democrat"
    else:
        party = "Republican"

    # state.party_affiliation = party


def fix_established(state):
    months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
    ]

    est = state.established
    est_split = est.split(" ")

    month = str(months.index(est_split[0]) + 1)
    if len(month) == 1:
        month = "0" + month

    date = est_split[1][:-1]
    if len(date) == 1:
        date = "0" + date

    year = est_split[2]

    sb = year + "-" + month + "-" + date

    # state.established = sb


def fix_location(state):
    northeast = [
        "Connecticut",
        "Maine",
        "Massachusetts",
        "New Hampshire",
        "Rhode Island",
        "Vermont",
        "New Jersey",
        "New York",
        "Pennsylvania",
    ]
    midwest = [
        "Illinois",
        "Indiana",
        "Michigan",
        "Ohio",
        "Wisconsin",
        "Iowa",
        "Kansas",
        "Minnesota",
        "Missouri",
        "Nebraska",
        "North Dakota",
        "South Dakota",
    ]
    south = [
        "Delaware",
        "Florida",
        "Georgia",
        "Maryland",
        "North Carolina",
        "South Carolina",
        "Virginia",
        "West Virginia",
        "Alabama",
        "Kentucky",
        "Mississippi",
        "Tennessee",
        "Arkansas",
        "Louisiana",
        "Oklahoma",
        "Texas",
    ]
    west = [
        "Arizona",
        "Colorado",
        "Idaho",
        "Montana",
        "Nevada",
        "New Mexico",
        "Utah",
        "Wyoming",
        "Alaska",
        "California",
        "Hawaii",
        "Oregon",
        "Washington",
    ]

    name = state.name
    loc = ""

    if name in northeast:
        loc = "Northeast"
    elif name in midwest:
        loc = "Midwest"
    elif name in south:
        loc = "South"
    elif name in west:
        loc = "West"

    # state.location = loc


# Add congressperson to list list of reps of state
def fix_num_proposed(state):
    abb = state.abbreviation

    people = db.session.query(Congressperson).filter_by(state=abb).all()
    counter = 0
    for i, person in enumerate(people):
        counter += person.bills_sponsored

    # state.num_proposed_bills = counter


def add_column():
    engine = create_engine(os.getenv("TEST_AWS_CONNECTION_KEY"))
    engine.execute("ALTER TABLE %s ADD COLUMN in_poverty_percentage FLOAT;" % "states")


def fix_states():
    states = db.session.query(State).all()
    # for state in states:
    # fix_poverty(state)
    # fix_afil(state)
    # fix_established(state)
    # fix_location(state)
    # fix_num_proposed(state)


def main():
    # add_column()
    # fix_states()
    # repair_poverty()

    db.session.commit()


if __name__ == "__main__":
    main()
