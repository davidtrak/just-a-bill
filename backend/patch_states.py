from datetime import date
from dotenv import load_dotenv
import os, requests
from api import db
from api.models import Congressperson, Bill, State
import json
from sqlalchemy import create_engine
import sys
import base64
from bs4 import BeautifulSoup

load_dotenv(override=True)

headers = {"X-API-Key": os.getenv("CONGRESS_API_KEY")}

# Hard code state affiliations because its hard to scrape
rep_states = [
    "Alabama",
    "Alaska",
    "Arizona",
    "Arkansas",
    "Florida",
    "Idaho",
    "Indiana",
    "Kansas",
    "Mississippi",
    "Missouri",
    "Montana",
    "Nebraska",
    "North Dakota",
    "Ohio",
    "Oklahoma",
    "South Carolina",
    "South Dakota",
    "Tennessee",
    "Texas",
    "Utah",
    "West Virginia",
    "Wyoming",
]

# Returns the state's name for this geo, None if it is invalid
def pull_state_name(geo):
    url = "https://datausa.io/api/data?measure=Poverty%20Population&year=latest&Geography=04000US{}".format(
        geo
    )

    res = requests.get(url)
    data = res.json()["data"][0]
    if "Geography" not in data:
        return None
    return data["Geography"]


# Takes in a datausa url and the number of people in poverty in this state. Returns None if not a valid state ID
def pull_state_poverty(geo):
    url = "https://datausa.io/api/data?measure=Poverty%20Population&year=latest&Geography=04000US{}".format(
        geo
    )

    res = requests.get(url)
    data = res.json()["data"][0]
    return data["Poverty Population"]


# Takes in a datausa url and returns the 5 most common ethnic groups, in order
def pull_state_ethnic(geo):
    url = "https://datausa.io/api/data?drilldowns=Race,Ethnicity&measures=Hispanic%20Population&year=latest&Geography=04000US{}".format(
        geo
    )

    res = requests.get(url)
    data = res.json()["data"]

    ethnic_dict = {}

    for element in data:
        race = element["Race"]
        pop = element["Hispanic Population"]
        eth = element["Ethnicity"]
        if eth == "Hispanic or Latino":
            race = eth
        if race in ethnic_dict:
            ethnic_dict[race] += pop
        else:
            ethnic_dict[race] = pop

    sorted_ed = sorted(ethnic_dict.items(), key=lambda x: x[1], reverse=True)[:5]

    sb = ""
    for pair in sorted_ed:
        sb += str(pair[0]) + "; "
    sb = sb[: len(sb) - 2]

    return sb


# Takes in a datausa url and returns median household income for the state
def pull_state_income(geo):
    url = "https://datausa.io/api/data?measure=Household%20Income%20by%20Race&year=latest&Geography=04000US{}".format(
        geo
    )

    res = requests.get(url)
    data = res.json()["data"][0]
    return data["Household Income by Race"]


# Add information about states that was not included in the back4app api
def pull_state_demographics():
    demo_dict = {}

    for i in range(1, 57):
        geo = str(i)
        if len(geo) == 1:
            geo = "0" + geo

        name = pull_state_name(geo)

        if name != None and name != "District of Columbia":
            in_poverty = pull_state_poverty(geo)
            ethnic_groups = pull_state_ethnic(geo)
            median_income = pull_state_income(geo)
            demo_dict[name] = {
                "in_poverty": in_poverty,
                "ethnic_groups": ethnic_groups,
                "median_household_income": median_income,
            }

    return demo_dict


# Use python beautifulsoup to scrape a description and party afil of each state
def scrape_state_data(oriname):
    name = oriname.split(" ")
    name = "-".join(name)
    # scrape description
    url = "https://datausa.io/profile/geo/{}".format(name.lower())
    page = requests.get(url)
    soup = BeautifulSoup(page.content, "html.parser")
    line = soup.find("div", {"class": "section-description"}).getText()
    line = line.split("About the photo:")[0]

    # derive affiliation
    if name == "Wisconsin":
        party_afil = "Even"
    else:
        party_afil = "Republican" if name in rep_states else "Democratic"

    return line, party_afil


# Add a state to the db
def load_entry(state, i):
    entry = dict()
    entry["id"] = i
    entry["name"] = state["name"]
    entry["flag"] = state["flag"]
    entry["abbreviation"] = state["postalAbreviation"]
    entry["capital"] = state["capital"]
    entry["largest_city"] = state["largestCity"]
    entry["established"] = state["established"]
    entry["population"] = state["population"]
    entry["size"] = state["totalAreaSquareMiles"]
    entry["num_congressmen"] = state["numberRepresentatives"]

    description, party_affiliation = scrape_state_data(entry["name"])
    entry["description"] = description
    entry["party_affiliation"] = party_affiliation

    return entry


# Pull all the states from the api
def pull_states():
    url = "https://parseapi.back4app.com/classes/Usstatesdataset_States?limit=50&order=name&excludeKeys=landAreaSquareKilometers,landAreaSquareMiles,totalAreaSquareKilometers,waterAreaSquareKilometers,waterAreaSquareMiles"
    headers = {
        "X-Parse-Application-Id": os.getenv("STATES_PARSE_APP_ID"),
        "X-Parse-REST-API-Key": os.getenv("STATES_PARSE_REST_API_KEY"),
    }
    res = requests.get(url=url, headers=headers)
    data = res.json()["results"]

    states_list = []

    demo_dict = pull_state_demographics()

    # Add each one with unique id : i
    for i, state in enumerate(data):
        entry = load_entry(state, i)
        name = entry["name"]

        for key, val in demo_dict.items():
            if key == name:
                entry = dict(entry, **val)

        states_list.append(entry)

    return states_list


# Given one entry and id, add to db
def add_state(entry, i):
    entry["id"] = i
    state_entry = State(**entry)

    db.session.add(state_entry)


# Reset a specific table
def reset_table():
    engine = create_engine(os.getenv("TEST_AWS_CONNECTION_KEY"))
    State.__table__.drop(engine)
    # engine.execute("DROP TABLE %s CASCADE;" % "congresspeople")
    State.__table__.create(engine)


def main():
    reset_table()
    states_list = pull_states()

    # Add each one with unique id : i
    for i, state in enumerate(states_list):
        add_state(state, i)
        print("added s {}".format(i))

    db.session.commit()


if __name__ == "__main__":
    main()
