from datetime import date
from dotenv import load_dotenv
import os, requests
from api import db
from api.models import Congressperson, Bill, State
import json
from sqlalchemy import create_engine
import sys
import base64

load_dotenv(override=True)

NUM_BILLS = 25

headers = {"X-API-Key": os.getenv("CONGRESS_API_KEY")}

specific_keys = [
    "bill_id",
    "bill_slug",
    "bill",
    "bill_type",
    "title",
    "short_title",
    "sponsor_title",
    "sponsor",
    "sponsor_id",
    "sponsor_party",
    "sponsor_state",
    "introduced_date",
    "active",
    "last_vote",
    "house_passage",
    "senate_passage",
    "enacted",
    "vetoed",
    "cosponsors",
    "primary_subject",
    "committees",
    "latest_major_action_date",
    "latest_major_action",
    "summary",
    "summary_short",
    "short_title",
]

# Generate pdf url
def get_pdf_url(bill):
    congress = "117"
    bill_str = congress + bill["bill_slug"]
    return f"https://www.govinfo.gov/content/pkg/BILLS-{bill_str}ih/pdf/BILLS-{bill_str}ih.pdf"


def load_entry(bill):
    bill_entry = {}
    bill_keys = bill.keys()

    for key in specific_keys:
        if key in bill_keys:
            bill_entry[key] = bill[key]
        else:
            bill_entry[key] = None

    pdf_url = get_pdf_url(bill)
    res = requests.get(pdf_url, headers)
    bill_entry["pdf_url"] = pdf_url if sys.getsizeof(res.content) != 45342 else None

    status_path = []
    for version in bill["versions"]:
        status_path.append(version["status"])

    bill_entry["status_path"] = ", ".join(status_path)

    return bill_entry


def pull_bills(target_bill_slugs):
    bills_list = []

    for slug in target_bill_slugs:
        url = "https://api.propublica.org/congress/v1/117/bills/{}.json".format(slug)
        res = requests.get(url=url, headers=headers)
        data = res.json()
        bill = data["results"][0]
        bills_list.append(load_entry(bill))
        print("appended slug {}".format(slug))

    return bills_list


# Returns a list of bill ids that have summary, short_summary, and pdfs associated with them
def get_complete_bills():
    # Increment of 20
    NUM_BILLS = 3000

    offset = 0
    complete_bills = []

    while offset < NUM_BILLS and len(complete_bills) < 400:
        print("offset = {}".format(offset))
        url = "https://api.propublica.org/congress/v1/117/both/bills/introduced.json?offset={}".format(
            offset
        )
        res = requests.get(url=url, headers=headers)
        data = res.json()
        data = data["results"][0]["bills"]

        for bill in data:
            pdf_url = get_pdf_url(bill)
            res = requests.get(pdf_url, headers)
            # Do some checks to make sure fields are not lame
            if (
                bill["summary"] != ""
                or bill["summary_short"] != ""
                or sys.getsizeof(res.content) != 45342
            ):
                complete_bills.append(bill["bill_slug"])

        offset += 20

    return complete_bills


# Given one entry and id, add to db
def add_bill(entry, i):
    entry["id"] = i
    bills_entry = Bill(**entry)

    # Link bill to state and congresspeople
    link_bill_to_state(entry["sponsor_state"], bills_entry)
    link_bill_to_congresspeople(entry["sponsor"], bills_entry)

    db.session.add(bills_entry)


# Add bill to state's bill list
def link_bill_to_state(state_abv, bill_entry):
    state = db.session.query(State).filter_by(abbreviation=state_abv).first()
    if state:
        state.bills_proposed.append(bill_entry)


# Add bill to congresspersons bill list
def link_bill_to_congresspeople(name, bill_entry):
    person = db.session.query(Congressperson).filter_by(full_name=name).first()
    if person:
        person.bills_proposed.append(bill_entry)


# Reset a specific table
def reset_table():
    engine = create_engine(os.getenv("TEST_AWS_CONNECTION_KEY"))
    Bill.__table__.drop(engine)
    # engine.execute("DROP TABLE %s CASCADE;" % "congresspeople")
    Bill.__table__.create(engine)


def main():
    reset_table()
    target_bill_slugs = get_complete_bills()
    print(target_bill_slugs)
    bills_list = pull_bills(target_bill_slugs)

    with open("bills.json", "w") as f:
        for item in bills_list:
            f.write("%s\n" % item)

    # Add each one with unique id : i
    for i, bill in enumerate(bills_list):
        add_bill(bill, i)
        print("added b {}".format(i))

    db.session.commit()


if __name__ == "__main__":
    main()
