import "./App.css";
import Splash from "Routes/Splash/Splash";
import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import BillsPage from "Routes/BillsModel/BillsPage";
import CongressPage from "Routes/CongressModel/CongressPage";
import StatePage from "Routes/StatesModel/StatePage";
import AboutPage from "Routes/About/AboutPage";
import MyNavbar from "Common/Navbar/MyNavbar";
import BillInstance from "Routes/BillInstance/BillInstance";
import CongressInstance from "Routes/CongressInstance/CongressInstance";
import StateInstance from "Routes/StateInstance/StateInstance";
import Search from "Routes/Search/Search";
import VisualizationPage from "Routes/Visualizations/Visualizations";

function App() {
  return (
    <div className="app-div">
      <BrowserRouter>
        <MyNavbar />
        <Routes>
          <Route path="/" element={<Splash />} />
          <Route path="/bills" element={<BillsPage />} />
          <Route path="/congresspeople" element={<CongressPage />} />
          <Route path="/states" element={<StatePage />} />
          <Route path="/bills/:id" element={<BillInstance />} />
          <Route path="/congresspeople/:id" element={<CongressInstance />} />
          <Route path="/states/:id" element={<StateInstance />} />
          <Route path="/about" element={<AboutPage />} />
          <Route path="/search" element={<Search />} />
          <Route path="/visualizations" element={<VisualizationPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
