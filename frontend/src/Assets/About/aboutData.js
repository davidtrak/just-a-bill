import ricky from "./Headshots/ricky.jpg";
import david from "./Headshots/david.jpg";
import connor from "./Headshots/connor.jpg";
import rigo from "./Headshots/rigo.jpg";
import augie from "./Headshots/augie.jpg";
import aws from "./Logos/aws.jpg";
import postman from "./Logos/postman.jpg";
import gitlab from "./Logos/gitlab.jpg";
import react from "./Logos/react.jpg";
import reactboostrap from "./Logos/react-boostrap.jpg";
import datausa from "./Logos/datausa.png";
import mediawiki from "./Logos/mediawiki.png"
import propublica from "./Logos/propublica.jpg";
import b4a from "./Logos/b4a.jpg";
import flask from "./Logos/flask.jpg";
import black from "./Logos/black.jpg";
import marsh from "./Logos/marsh.jpg";
import selenium from "./Logos/selenium.jpg";
import jest from "./Logos/jest.jpg";
import docker from "./Logos/docker.jpg";
import prettier from "./Logos/prettier.jpg";
import google from "./Logos/google.jpg";

const ourData = [
  {
    title: "Postman",
    description: "Our API Design and Documentation",
    img: postman,
    url: "https://documenter.getpostman.com/view/19803776/UVyyutXa",
  },
  {
    title: "GitLab",
    description: "Our repo",
    img: gitlab,
    url: "https://gitlab.com/davidtrak/just-a-bill",
  },
];

const apiData = [
  {
    title: "ProPublica Congress API",
    description: "Data on Congresspeople and bills",
    img: propublica,
    url: "https://www.propublica.org/datastore/api/propublica-congress-api",
  },
  {
    title: "Back4App States API",
    description: "States general data",
    img: b4a,
    url: "https://www.back4app.com/database/back4app/usstates/dataset",
  },
  {
    title: "Data USA",
    description: "Additional state information",
    img: datausa,
    url: "https://datausa.io/about/api/",
  },
  {
    title: "MediaWiki",
    description: "Congressperson descriptions",
    img: mediawiki,
    url: "https://www.mediawiki.org/wiki/API:Main_page#Endpoint",
  },
  {
    title: "Google Maps API",
    description: "Embedded map of state",
    img: google,
    url: "https://developers.google.com/maps/documentation/embed/get-started",
  },
];

const toolData = [
  {
    title: "AWS",
    description: "Cloud Hosting Service",
    img: aws,
    url: "https://aws.amazon.com/",
  },
  {
    title: "Postman",
    description: "API Design and Documentation",
    img: postman,
    url: "https://www.postman.com/",
  },
  {
    title: "GitLab",
    description: "Git repository",
    img: gitlab,
    url: "https://about.gitlab.com/",
  },
  {
    title: "React",
    description: "Frontend JS Library",
    img: react,
    url: "https://reactjs.org/",
  },
  {
    title: "React Bootstrap",
    description: "React UI Library",
    img: reactboostrap,
    url: "https://react-bootstrap.github.io/",
  },
  {
    title: "Flask",
    description: "Backend Framework",
    img: flask,
    url: "https://palletsprojects.com/p/flask/",
  },
  {
    title: "Marshmallow",
    description: "Validation Library",
    img: marsh,
    url: "https://marshmallow.readthedocs.io/en/stable/#",
  },
  {
    title: "Black",
    description: "Python Code Formatter",
    img: black,
    url: "https://black.readthedocs.io/en/stable/",
  },
  {
    title: "Jest",
    description: "JavaScript Test Framework",
    img: jest,
    url: "https://jestjs.io/",
  },
  {
    title: "Selenium",
    description: "Automated GUI Testing Framework",
    img: selenium,
    url: "https://www.selenium.dev/",
  },
  {
    title: "Docker",
    description: "Containerization tool",
    img: docker,
    url: "https://www.docker.com/",
  },
  {
    title: "Prettier",
    description: "JavaScript Code Formatter",
    img: prettier,
    url: "https://prettier.io/",
  },

];
const bioData = [
  {
    name: "Rigo Garcia",
    role: "Frontend Developer",
    leadership_role: "Phase 1 Leader",
    bio: "I am a third year CS major at UT Austin. I am from Brownsville, TX and some of my hobbies are playing table tennis, regular tennis, watching movies, and spending time with family and friends. My favorite place to eat in Austin is Homeslice Pizza!",
    image: rigo,
    commits: 0,
    issues: 0,
    unit_tests: 15,
  },

  {
    name: "David Trakhtengerts",
    role: "Full Stack Developer",
    leadership_role: "Phase 2 Leader",
    bio: "I am a third year CS major and Business minor at UT Austin, graduating in Fall 2023. I am from a suburb of Dallas, Texas. I like to play volleyball, travel, play video games, and spend time with my family and friends. I also got in to F1 last season so I am very excited for the 2022 season to start up soon.",
    image: david,
    commits: 0,
    issues: 0,
    unit_tests: 17,
  },
  {
    name: "Connor Popp",
    role: "Frontend Developer",
    leadership_role: "Phase 3 Leader",
    bio: "I am a second year CS major at UT Austin, graduating in 2023 and minoring in Business. I grew up in a small town that you’ve probably never heard of known as Hallettsville, Texas. Apart from programming I enjoy hiking, playing piano, video games, table tennis, and spending time with friends. If you know me at all you’ll also know that I’m a total sucker for all things pasta and Chick-fil-a.",
    image: connor,
    commits: 0,
    issues: 0,
    unit_tests: 5,
  },
  {
    name: "Ricky Woodruff",
    role: "Full Stack Developer",
    leadership_role: "Phase 4 Leader",
    bio: "I am a 3rd year CS major at UT Austin, graduating in Fall 2022 and potentially minoring in Economics. I'm from Winter Park, Florida, and I attended Trinity Preparatory School. I enjoy running, playing board games and video games, playing acoustic guitar, table tennis, and hanging out with friends. I'll be joining Stripe full-time as a Software Engineer after I graduate in the Fall.",
    image: ricky,
    commits: 0,
    issues: 0,
    unit_tests: 7,
  },
  {
    name: "Audie Bethea",
    role: "Full Stack Developer",
    leadership_role: "",
    bio: "I’m a 4th year CS major at UT Austin, working at Palo Alto Networks as a software engineer after graduation. I’m from Dallas, Texas, and I’m passionate in my belief that Dallas is the 2nd best city in Texas (after Austin, of course). In my free time, I like to write music, play pickleball, beat my friends in board games, and geek out about Survivor. Lately, I’ve been cooking a lot too- turns out it's a lot more fun when your food tastes good!",
    image: augie,
    commits: 0,
    issues: 0,
    unit_tests: 7,
  },
];

export { bioData, toolData, apiData, ourData };
