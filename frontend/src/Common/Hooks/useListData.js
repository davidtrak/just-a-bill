// @flow

import React, { useState, useEffect } from "react";
import { fetchListData } from "Library/Data";
import type { Endpoint } from "Library/Data";
import type { DataModel } from "Library/DataModels";

export default function useListData(
  source: Endpoint,
  currentPage: number = 1,
  resultsPerPage: number = 20,
  filterString: string = "",
  sortBy: string = "",
  searchFor: string = ""
): [{ totalResults: number, page: Array<DataModel> } | null, boolean, boolean] {
  const [data, setData] = useState({ totalResults: 0, page: [] });
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    const load = async () => {
      // TODO - Handle errors here
      setIsLoading(true);
      setData(
        await fetchListData(
          source,
          currentPage,
          resultsPerPage,
          filterString,
          sortBy,
          searchFor
        )
      );
      setIsLoading(false);
    };
    load();
  }, [source, currentPage, resultsPerPage, filterString, sortBy, searchFor]);

  return [data, isLoading, isError];
}
