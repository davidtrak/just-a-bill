// @flow

import { useEffect } from "react";
import Mark from "mark.js";

export default function useHighlightTerms(
  searchString: string,
  componentSelector: string,
  dependencies: Array<any>,
  options: {} = {},
): void {
  useEffect(() => {
    let instance = new Mark(
      document.querySelectorAll(componentSelector)
    );
    instance.unmark();
    instance.mark(searchString, options);
  }, [...dependencies]);
}
