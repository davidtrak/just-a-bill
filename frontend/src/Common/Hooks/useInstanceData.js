// @flow

import { useState, useEffect } from "react";
import { fetchSingleData } from "Library/Data.js";
import type { DataModel } from "Library/DataModels";

export default function useInstanceData(
  source: string,
  id: string
): [DataModel | null, boolean, boolean] {
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    const load = async () => {
      // TODO - Handle errors here
      setIsLoading(true);
      setData(await fetchSingleData(source, id));
      setIsLoading(false);
    };
    load();
  }, [id, source]);

  return [data, isLoading, isError];
}
