// @flow

import * as React from "react";
import { Card, ListGroup, ListGroupItem } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import type { BillModel } from "Library/DataModels";

type BillCardProps = {
  id: string,
  data: BillModel,
};

const BillCard = (props: BillCardProps): React.Node => {
  let navigate = useNavigate();

  let navToInstance = () => {
    let path = "/bills/" + props.id;
    navigate(path);
  };

  return (
    <Card
      border="primary"
      style={{ width: "18rem", cursor: "pointer" }}
      onClick={() => {
        navToInstance();
      }}
    >
      {/* <Card.Img variant="top" src="holder.js/100px180?text=Image cap" /> */}
      <Card.Body>
        <Card.Title>{props.data.number}</Card.Title>
        <Card.Text>{props.data.shortTitle}</Card.Text>
      </Card.Body>
      <ListGroup className="list-group-flush">
        <ListGroupItem>
          Status: {props.data.active ? "Active" : "Inactive"}
        </ListGroupItem>
        <ListGroupItem>Sponsor: {props.data.sponsor}</ListGroupItem>
        <ListGroupItem>Committee: {props.data.committees}</ListGroupItem>
      </ListGroup>
    </Card>
  );
};

export default BillCard;
