// @flow

import * as React from "react";
import { Card, ListGroup, ListGroupItem } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import type { StateModel } from "Library/DataModels";

import "./StateCard.css";

type StateCardProps = {
  data: StateModel,
};

const StateCard = (props: StateCardProps): React.Node => {
  let navigate = useNavigate();

  let navToInstance = () => {
    let path = "/states/" + props.data.id;
    console.log(path);
    navigate(path);
  };

  // https://github.com/oxguy3/flags
  // NOTE: there is a repo with flags of all states even cities, in the world.. also has a HTTP end point(?), maybe use axios later
  return (
    <Card
      border="dark"
      style={{ width: "18rem", cursor: "pointer" }}
      onClick={() => {
        navToInstance();
      }}
      className="state-card"
    >
      <Card.Img
        variant="top"
        src={`https://raw.githubusercontent.com/CivilServiceUSA/us-states/master/images/flags/${props.data.name.toLowerCase().replace(" ", "-")}-large.png`}
        className="state-card-image"
      />
      <Card.ImgOverlay className="state-card-overlay">
        <Card.Title className="state-card-title">
          {props.data.name}
        </Card.Title>
      </Card.ImgOverlay>
      <ListGroup className="list-group-flush">
        <ListGroupItem>
          {`Majority Party: ${props.data.party}`}
        </ListGroupItem>
        <ListGroupItem>
          {`${props.data.numRepresentatives} Representatives`}
        </ListGroupItem>
        <ListGroupItem>
          {`Population: ${props.data.population}`}
        </ListGroupItem>
        <ListGroupItem>
          {`Physical Size: ${props.data.size} sq. miles`}
          <br />
        </ListGroupItem>
      </ListGroup>
    </Card>
  );
};

export default StateCard;
