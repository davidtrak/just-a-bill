import React from "react";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import StateCard from "./StateCard";
import { StateModel } from "Library/DataModels";

const mockedUsedNavigate = jest.fn();

configure({ adapter: new Adapter() });

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => mockedUsedNavigate,
}));

const defaultProps = {
  0: {
    abbreviation: "AR",
    capital: "Little Rock",
    cities: null,
    demographics: null,
    description:
      "In 2019, Arkansas had a population of 3.02M people with a median age of 38.8 and a median household income of $48,952. Between 2018 and 2019 the population of Arkansas grew from 3.01M to 3.02M, a 0.132% increase and its median household income grew from $47,062 to $48,952, a 4.02% increase.The 5 largest ethnic groups in Arkansas are  White (Non-Hispanic) (72%), Black or African American (Non-Hispanic) (15.4%), White (Hispanic) (4.74%), Other (Hispanic) (2.4%), and Two+ (Non-Hispanic) (2.36%). 0% of the households in Arkansas speak a non-English language at home as their primary language.96.8% of the residents in Arkansas are U.S. citizens.The largest universities in Arkansas are University of Arkansas (6,818 degrees awarded in 2019), Arkansas State University-Main Campus (5,009 degrees), and Arkansas Tech University (4,040 degrees).In 2019, the median property value in Arkansas was $136,200, and the homeownership rate was 65.5%. Most people in Arkansas drove alone to work, and the average commute time was 21.2 minutes. The average car ownership in Arkansas was 2 cars per household.Arkansas borders Louisiana, Mississippi, Missouri, Oklahoma, Tennessee, and Texas.",
    established: "Jun 15, 1836",
    ethnic_groups:
      "White Alone, Black or African American Alone, Hispanic or Latino, Two or More Races, Asian Alone",
    flag: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Flag_of_Arkansas.svg/23px-Flag_of_Arkansas.svg.png",
    id: 3,
    in_poverty: 2910954,
    largest_city: "Little Rock",
    location: null,
    median_household_income: 48952,
    name: "Arkansas",
    num_congressmen: 4,
    num_proposed_bills: null,
    party_affiliation: "Republican",
    population: 3004279,
    size: 53179,
  },
};

test("renders state card info", () => {
  const stateData = new StateModel(defaultProps[0]);
  const wrapper = mount(<StateCard data={stateData} />);
  expect(wrapper.find(".state-card-image").first().prop("src")).toEqual(
    "https://raw.githubusercontent.com/CivilServiceUSA/us-states/master/images/flags/arkansas-large.png"
  );
  expect(wrapper.text()).toContain("Arkansas");
  expect(wrapper.text()).toContain("Majority Party: Republican");
  // expect(wrapper.text()).toContain("Location:");
  expect(wrapper.text()).toContain("4 Representatives");
  expect(wrapper.text()).toContain("53179 sq. miles");
  // expect(wrapper.text()).toContain("1 Bills Proposed");
});
