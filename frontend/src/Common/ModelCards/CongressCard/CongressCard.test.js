import React from "react";
import { mount, configure } from "enzyme";
import CongressCard from "./CongressCard";
import Adapter from "enzyme-adapter-react-16";
import { CongresspersonModel } from "Library/DataModels";

const mockedUsedNavigate = jest.fn();

configure({ adapter: new Adapter() });

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => mockedUsedNavigate,
}));

const defaultProps = {
  0: {
    age: 60,
    bills_proposed: [],
    district: null,
    facebook_account: "senatortammybaldwin",
    first_name: "Tammy",
    full_name: "Tammy Baldwin",
    gender: "F",
    headshot_url: "https://unavatar.io/SenatorBaldwin",
    id: 0,
    in_office: true,
    last_name: "Baldwin",
    middle_name: null,
    next_election: "2024",
    party: "D",
    seniority: 9,
    state_id: 48,
    state_name: "Florida",
    title: "Senator",
    terms_served: 12,
    twitter_account: "SenatorBaldwin",
    youtube_account: "witammybaldwin",
  },
};

test("renders congress card info", () => {
  const congressData = new CongresspersonModel(defaultProps[0]);
  const wrapper = mount(<CongressCard data={congressData} />);
  expect(wrapper.find(".cong-card-image").first().prop("src")).toEqual(
    "https://unavatar.io/SenatorBaldwin"
  );
  expect(wrapper.text()).toContain("Tammy Baldwin");
  expect(wrapper.text()).toContain("Senator");
  expect(wrapper.text()).toContain("Democrat");
  expect(wrapper.text()).toContain("In Office");
  expect(wrapper.text()).toContain("Seniority: 9");
});
