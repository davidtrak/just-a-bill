// @flow

import * as React from "react";
import { Card, ListGroup, ListGroupItem } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import type { CongresspersonModel } from "Library/DataModels";

import "./CongressCard.css";

const CongressCard = (props: { data: CongresspersonModel }): React.Node => {
  let navigate = useNavigate();

  let navToInstance = () => {
    let path = "/congresspeople/" + props.data.id;
    console.log(path);
    navigate(path);
  };

  return (
    <Card
      border="dark"
      style={{ width: "18rem", cursor: "pointer" }}
      onClick={() => {
        navToInstance();
      }}
      className="cong-card"
    >
      <Card.Img
        variant="top"
        src={props.data.headshotUrl}
        className="cong-card-image"
      />
      <Card.ImgOverlay
        className={
          props.data.party !== "Republican" ? "cong-card-dem" : "cong-card-rep"
        } // TODO I know there are more parties, I will put them in an array or something once we get all the data
      >
        <Card.Title className="cong-card-title">
          {props.data.fullName}
        </Card.Title>
        <Card.Subtitle className="cong-card-sub">
          {props.data.title}
        </Card.Subtitle>
      </Card.ImgOverlay>
      <ListGroup className="list-group-flush">
        <ListGroupItem>
          {props.data.state}
          {props.data.district !== null ? " - District " : ""}
          {props.data.district}
        </ListGroupItem>
        <ListGroupItem>
          {props.data.party}
        </ListGroupItem>
        <ListGroupItem>
          {props.data.inOffice ? "In Office" : "Not In Office"}
        </ListGroupItem>
        <ListGroupItem>
          Seniority: {props.data.seniority}
        </ListGroupItem>
      </ListGroup>
    </Card>
  );
};

export default CongressCard;
