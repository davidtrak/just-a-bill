// @flow
import * as React from "react";
import { useState, useEffect, useRef } from "react";
import { FormControl, InputGroup, Button } from "react-bootstrap";

type Props = {
  placeholderText: string,
  initialValue: string,
  autoSubmit: boolean,
  onSubmit: (search: string) => void,
};

export default function SearchBar(props: Props): React.Node {
  const [val, setVal] = useState(props.initialValue);
  const isMounted = useRef(false);

  useEffect(() => {
    if (isMounted.current) {
      if (props.autoSubmit) {
        const delayFn = setTimeout(() => {
          props.onSubmit(val);
        }, 750);

        return () => clearTimeout(delayFn);
      } else {
        return;
      }
    } else {
      isMounted.current = true;
    }
  }, [val]);

  return (
    <InputGroup className="search-bar">
      <InputGroup.Text style={{ padding: "0 .75rem 0 0.5rem" }}>
        <span
          style={{
            WebkitTransform: "rotate(-45deg)",
            MozTransform: "rotate(-45deg)",
            OTransform: "rotate(-45deg)",
            transform: "rotate(-45deg)",
            WebkitUserSelect: "none",
            MozUserSelect: "none",
            OUserSelect: "none",
            UserSelect: "none",
            fontSize: "1.3rem",
          }}
          dangerouslySetInnerHTML={{ __html: "&#9906;" }}
        />
      </InputGroup.Text>
      <FormControl
        placeholder={props.placeholderText}
        aria-label="Search"
        type="search"
        value={val}
        onChange={(event) => {
          setVal(event.target.value);
        }}
        onKeyDown={(event) => {
          if (event.key === "Enter") {
            props.onSubmit(val);
          }
        }}
      />
      {!props.autoSubmit ? (
        <Button
          variant="outline-secondary"
          onClick={() => {
            props.onSubmit(val);
          }}
        >
          Submit
        </Button>
      ) : (
        ""
      )}
    </InputGroup>
  );
}

SearchBar.defaultProps = {
  placeholderText: "Search...",
  initialValue: "",
  autoSubmit: false,
  onSubmit: (search: string) => {
    console.log(`Search string submitted as "${search}"`);
  },
};
