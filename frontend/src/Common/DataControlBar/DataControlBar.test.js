import React from "react";
import { mount } from "enzyme";
import { act } from "react-dom/test-utils";
import DataControlBar from "./DataControlBar";

const testFilterHeader = {
  Party: {
    queryParam: "party",
    options: ["Democrat", "Republican"],
  },
  Chamber: {
    queryParam: "short_title",
    options: {
      Senate: "Sen.",
      House: "Rep.",
    },
  },
};

const testSortHeader: SortHeader = {
  Name: "full_name",
  Age: "age",
};

test("Properly renders filter and sort dropdowns", () => {
  const wrapper = mount(
    <DataControlBar
      filterHeader={testFilterHeader}
      selectedFilters={{ Party: new Set(["Democrat"]), Chamber: new Set() }}
      onFilterChange={() => {}}
      sortHeader={testSortHeader}
      sortBy={"Name"}
      onSortByChange={() => {}}
    />
  );

  expect(wrapper.find("FilterChip").length).toEqual(2);
  expect(wrapper.find("FilterChip").at(0).text()).toContain("Party");
  expect(wrapper.find("FilterChip").at(0).text()).toContain("1 selected");
  expect(wrapper.find("FilterChip").at(1).text()).toContain("Chamber");
  expect(wrapper.find("SortChip").first().text()).toContain("Name");

  wrapper.find("span.dropdown-toggle").at(0).simulate("click");
  wrapper.update();

  expect(wrapper.find(".dropdown-menu").first().text()).toContain("Democrat");
  expect(wrapper.find(".dropdown-menu").first().text()).toContain("Republican");
});

test("Returns proper filterstring, sortby, and search on fields clicked", () => {
  const sortChange = jest.fn();

  let wrapper = mount(
    <DataControlBar
      filterHeader={testFilterHeader}
      selectedFilters={{ Party: new Set(["Democrat"]), Chamber: new Set() }}
      onFilterChange={()=>{}}
      sortHeader={testSortHeader}
      sortBy={"Name"}
      onSortByChange={sortChange}
    />
  );

  act(() => {
    wrapper.find("span.btn").at(3).simulate("click");
    wrapper.update();
  });

  expect(sortChange).toHaveBeenCalledWith("-Name");
});
