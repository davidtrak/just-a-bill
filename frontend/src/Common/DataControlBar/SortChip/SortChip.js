// @flow

import * as React from "react";
import { Dropdown, Form, Button } from "react-bootstrap";
import PillButton from "Common/PillButton/PillButton";
import "./SortChip.css";

type Props = {
  options: Array<string>,
  selected: string,
  isDescending: boolean,
  onChange: (selection: string, isDescending: boolean) => void,
};

const SortChip = (props: Props): React.Node => {
  return (
    <div className="sort-chip">
      <Dropdown title="Sort">
        <Dropdown.Toggle as={PillButton}>
          {props.selected}
        </Dropdown.Toggle>

        <Dropdown.Menu className="menu" align="end">
          {props.options
            .filter((option) => option !== props.selected)
            .map((option) => (
              <Dropdown.Item
                key={option}
                onClick={() => props.onChange(option, false)}
              >
                {option}
              </Dropdown.Item>
            ))}
        </Dropdown.Menu>
      </Dropdown>
      <PillButton
        onClick={() => {
          props.onChange(props.selected, !props.isDescending);
        }}
      >
        <span
          dangerouslySetInnerHTML={{
            __html: props.isDescending ? "&downarrow;" : "&uparrow;",
          }}
        />
      </PillButton>
    </div>
  );
};

SortChip.defaultProps = {
  options: ["Option 1", "Option 2", "Option 3"],
  selected: "Option 1",
  isDescending: false,
  onChange: (selection: string, isDescending: boolean) => {
    console.log(`Selected ${selection} (${isDescending ? "Desc" : "Asc"})`);
  },
};

export default SortChip;
