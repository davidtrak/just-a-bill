// @flow

import * as React from "react";
import { useState } from "react";
import { Container, Button, Badge } from "react-bootstrap";
import FilterChip from "./FilterChip/FilterChip";
import SortChip from "./SortChip/SortChip";
import SearchBar from "Common/SearchBar/SearchBar";
import PillButton from "Common/PillButton/PillButton";
import type { FilterHeader, SortHeader } from "Library/FilterSortHeaders";
import "./DataControlBar.css";

type Props = {
  filterHeader: FilterHeader,
  selectedFilters: { [filterTitle: string]: Set<string> },
  onFilterOptionsChange: (
    filterFieldName: string,
    newSelectedOptions: Set<string>
  ) => void,
  sortHeader: SortHeader,
  sortBy: string,
  onSortByChange: (newSortBy: string) => void,
  searchFor: string,
  onSearchChange: (newSearch: string) => void,
};

const DataControlBar = (props: Props) => {
  return (
    <Container className="data-control-bar">
      <SearchBar
        onSubmit={(newSearch) => {
          props.onSearchChange(newSearch);
        }}
        initialValue={props.searchFor}
        autoSubmit
      />
      <hr />
      <div className="filter-sort-row">
        <div className="filters">
          {Object.entries(props.filterHeader).map(([key, val]) => (
            <FilterChip
              key={key}
              title={key}
              options={
                Array.isArray(val.options)
                  ? val.options
                  : Object.keys(val.options)
              }
              selected={props.selectedFilters[key] ?? new Set()}
              onSelectedChange={(newSelected) => {
                props.onFilterOptionsChange(key, newSelected);
              }}
            />
          ))}
        </div>
        <SortChip
          options={Object.keys(props.sortHeader)}
          selected={
            props.sortBy[0] === "-" ? props.sortBy.substr(1) : props.sortBy
          }
          isDescending={props.sortBy[0] === "-"}
          onChange={(selection: string, isDescending: boolean) =>
            props.onSortByChange(`${isDescending ? "-" : ""}${selection}`)
          }
        />
      </div>
    </Container>
  );
};

export default DataControlBar;
