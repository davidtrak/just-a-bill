// @flow

import * as React from "react";
import { Dropdown, Form, Button, Badge } from "react-bootstrap";
import PillButton from "Common/PillButton/PillButton";
import "./FilterChip.css";

type Props = {
  title: string,
  options: Array<string>,
  selected: Set<string>,
  onSelectedChange: (newSelected: Set<string>) => void,
};

const FilterChip = (props: Props): React.Node => {
  var numSelected = props.selected.size;
  return (
    <Dropdown title={props.title}>
      <Dropdown.Toggle
        as={PillButton}
        // pill
        bg={numSelected > 0 ? "primary" : "secondary"}
      >
        {props.title + (numSelected > 0 ? ` (${numSelected} selected)` : "")}
      </Dropdown.Toggle>

      <Dropdown.Menu
        className="menu"
        align="end"
      >
        <Dropdown.ItemText className="menu-list">
          {props.options.map((option) => (
            <li key={option}>
              <div className="menu-item">
                <Form.Label>{option}</Form.Label>
                <Form.Check
                  checked={props.selected.has(option)}
                  onChange={() => {
                    let newSelected = new Set([...props.selected]);
                    props.selected.has(option)
                      ? newSelected.delete(option)
                      : newSelected.add(option);
                    props.onSelectedChange(newSelected);
                  }}
                  inline
                />
              </div>
            </li>
          ))}
        </Dropdown.ItemText>
        {props.selected.size > 0 && (
          <>
            <Dropdown.Divider />
            <Dropdown.ItemText>
              <Button
                id="clear-btn"
                variant="link"
                onClick={() => {
                  props.onSelectedChange(new Set());
                }}
              >
                Clear Selected
              </Button>
            </Dropdown.ItemText>
          </>
        )}
      </Dropdown.Menu>
    </Dropdown>
  );
};

FilterChip.defaultProps = {
  title: "FilterChip",
  options: ["Option 1", "Option 2", "Option 3"],
  selected: (new Set(["Option 2"]): Set<string>),
  onCheck: (key: string, value: boolean): void =>
    console.log(`${value ? "Selected" : "Deselected"} ${key}`),
};

export default FilterChip;
