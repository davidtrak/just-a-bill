// @flow

import * as React from "react";
import { Badge, Button } from "react-bootstrap";
import "./PillButton.css";

type Props = {
  className?: string,
  bg?:
    | "primary"
    | "secondary"
    | "success"
    | "danger"
    | "warning"
    | "info"
    | "light"
    | "dark",
  text?:
    | "primary"
    | "secondary"
    | "success"
    | "danger"
    | "warning"
    | "info"
    | "light"
    | "dark",
  onClick: () => void,
  children: React.ChildrenArray<any>,
};

const PillButton = (props: Props): React.Node => {
  return (
    <div className="pill-button-wrapper">
      <Button
        className={"pill-button " + (props.className ?? "")}
        as={Badge}
        pill
        bg={props.bg}
        text={props.text}
        onClick={props.onClick}
      >
        {props.children}
      </Button>
    </div>
  );
};

PillButton.defaultProps = {
  className: "",
  onClick: () => {
    console.log("Clicked PillButton");
  },
};

export default PillButton;
