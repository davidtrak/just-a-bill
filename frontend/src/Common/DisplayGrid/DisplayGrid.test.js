import React from "react";
import { shallow, render, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import DisplayGrid from "./DisplayGrid";

configure({ adapter: new Adapter() });

const testData = [
  {
    text: "Hello",
    id: "0",
  },
  {
    text: "Hello",
    id: "1",
  },
  {
    text: "Hello",
    id: "2",
  },
];

const TestComponent = (props) => {
  return (
    <p>
      {props.data.text} + {props.data.id}
    </p>
  );
};

test("DisplayGrid renders correct number of components based on data", () => {
  const wrapper = shallow(<DisplayGrid data={testData} card={TestComponent} />);
  expect(wrapper.find(TestComponent)).toHaveLength(3);
});

test("DisplayGrid correctly passes data through props", () => {
  const wrapper = render(<DisplayGrid data={testData} card={TestComponent} />);
  expect(wrapper.text()).toContain("Hello + 0");
  expect(wrapper.text()).toContain("Hello + 1");
  expect(wrapper.text()).toContain("Hello + 2");
});
