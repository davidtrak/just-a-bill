import "./DisplayGrid.css";

import React from "react";
import type { Node } from "react";

type Props = {
  data: { id: string, ... }[],
  card: ComponentType<{ id: string, data: { ... } }>,
};

const DisplayGrid = (props: Props): Node => {
  return (
    <div className="display-grid">
      {props.data.map((value) => (
        <props.card
          key={value.id}
          id={value.id}
          data={value}
        />
      ))}
    </div>
  );
};

export default DisplayGrid;
