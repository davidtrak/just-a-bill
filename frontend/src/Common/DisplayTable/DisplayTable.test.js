import React from "react";
import { mount, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import DisplayTable from "./DisplayTable";

const mockedUsedNavigate = jest.fn();

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => mockedUsedNavigate,
}));

configure({ adapter: new Adapter() });

const headerData = {
  Bill: "number",
  Title: "short_title",
  Sponsor: "sponsor_name",
  Committees: "committees",
  "Introduced On": "introduced_date",
};

const defaultProps = {
  0: [
    {
      committees: "House Transportation and Infrastructure Committee",
      introduced_date: "2022-03-24",
      number: "H.R.7206",
      short_title: "Protecting Communities from Harmful Algal Blooms Act",
      sponsor_name: "Byron Donalds",
      id: 0,
    },
  ],
};

test("renders display table component", () => {
  const wrapper = mount(
    <DisplayTable header={headerData} data={defaultProps[0]} />
  );
  expect(wrapper.text()).toContain(
    "Protecting Communities from Harmful Algal Blooms Act"
  );
  expect(wrapper.text()).toContain("Byron Donalds");
  expect(wrapper.text()).toContain(
    "House Transportation and Infrastructure Committee"
  );
  expect(wrapper.text()).toContain("2022-03-24");
  expect(wrapper.text()).toContain("H.R.7206");
});
