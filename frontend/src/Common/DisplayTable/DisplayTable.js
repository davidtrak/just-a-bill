// @flow

import React from "react";
import type { Node } from "react";
import { Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

import "./DisplayTable.css";

type Props = {
  pathToInstance: string,
  header: { [string]: string },
  data: { id: string, ... }[],
};

// Takes in a Dictionary like ListOfBills, displays all attributes as a table.
// Gets the header attributes from the first entry, might want to change later
// to a dedicated Object Type or something.
const DisplayTable = (props: Props): Node => {
  let navigate = useNavigate();

  let navToInstance = (id: string) => {
    let path = props.pathToInstance + id;
    console.log(path);
    navigate(path);
  };

  return (
    <React.Fragment>
      <Table responsive hover>
        <thead className={props.stateInst ? "dt-text" : ""}>
          <tr>
            {Object.keys(props.header).map((columnName) => (
              <th key={columnName}>{columnName}</th>
            ))}
          </tr>
        </thead>
        <tbody className={props.stateInst ? "dt-text" : ""}>
          {props.data.map((value) => (
            <tr
              key={value.id}
              onClick={() => navToInstance(value.id)}
              className="display-table-row"
            >
              {Object.keys(props.header).map((columnName) => (
                <td key={columnName + ":" + value.id.toString()}>
                  {columnName.toString().toLowerCase().includes("image") ? (
                    <img
                      // $FlowIgnore[incompatible-use]
                      src={value[props.header[columnName]]}
                      className="display-table-image"
                      alt=""
                    />
                  ) : value[props.header[columnName]] !== undefined ? (
                    // $FlowIgnore[incompatible-use]
                    value[props.header[columnName]]
                  ) : (
                    "Undefined"
                  )}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </Table>
    </React.Fragment>
  );
};

export default DisplayTable;
