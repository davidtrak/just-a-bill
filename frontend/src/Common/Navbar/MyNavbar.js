import React from "react";
import { Container, Nav, Navbar } from "react-bootstrap";

const MyNavbar = () => {
  return (
    <>
      <Navbar bg="dark" expand="sm" variant="dark" sticky="top">
        <Container>
          <Navbar.Brand href="/">justabill</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="justify-content-start">
              <Nav.Link href="/states">States</Nav.Link>
              <Nav.Link href="/bills">Bills</Nav.Link>
              <Nav.Link href="/congresspeople">Congresspeople</Nav.Link>
              <Nav.Link href="/search">Search</Nav.Link>
              <Nav.Link href="/visualizations">Visualizations</Nav.Link>
              <Nav.Link href="/about">About</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default MyNavbar;
