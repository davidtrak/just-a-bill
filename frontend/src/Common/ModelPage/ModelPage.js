// @flow

import "./ModelPage.css";

import React, { cloneElement } from "react";
import type { Node, Element } from "react";
import {
  Container,
  Row,
  Spinner,
  Dropdown,
  DropdownButton,
} from "react-bootstrap";
import { useSearchParams } from "react-router-dom";
import useListData from "Common/Hooks/useListData";
import useHighlightTerms from "Common/Hooks/useHighlightTerms";
import PageControl from "Common/ModelPage/PageControl/PageControl";
import DataControlBar from "Common/DataControlBar/DataControlBar";
import type { FilterHeader, SortHeader } from "Library/FilterSortHeaders";
import type { Endpoint } from "Library/Data";

type Props = {
  title: string,
  resultsPerPage: number,
  filterHeader: FilterHeader,
  sortHeader: SortHeader,
  body: Element<any>,
  dataSource: Endpoint,
};

// Takes a filter state object and converts it into a string
// formatted for use in an html query
// Example return: "state=Texas&state=Alabama&title=Senator"
const constructFilterString = (
  filterHeader: FilterHeader,
  filters: {
    [fieldName: string]: Set<string>,
  }
): string => {
  let filterString = "";
  for (const [filter, selected] of Object.entries(filters)) {
    let header = filterHeader[filter];
    for (const option of selected) {
      filterString += `&${header.queryParam}=${
        Array.isArray(header.options) ? option : header.options[option]
      }`;
    }
  }
  // Remove first &
  return filterString.substring(1);
};

const ModelPage = (props: Props): Node => {
  const [searchParams, setSearchParams] = useSearchParams();

  const currentPage = !isNaN(parseInt(searchParams.get("page")))
    ? parseInt(searchParams.get("page"))
    : 1;
  const perPage = !isNaN(parseInt(searchParams.get("perPage")))
    ? parseInt(searchParams.get("perPage"))
    : props.resultsPerPage;
  const sortBy =
    searchParams.get("sort") ?? Object.keys(props.sortHeader)[0] ?? "";
  const searchFor = searchParams.get("search") ?? "";
  const filters = Object.fromEntries(
    Array.from(searchParams.keys())
      .map((key) => [key, new Set<string>(searchParams.getAll(key))])
      .filter((val) => !["page", "perPage", "sort", "search"].includes(val[0]))
  );

  const [data, isLoading, isError] = useListData(
    props.dataSource,
    currentPage,
    perPage,
    constructFilterString(props.filterHeader, filters),
    (sortBy[0] === "-" ? "-" : "") +
      props.sortHeader[sortBy[0] === "-" ? sortBy.substr(1) : sortBy],
    searchFor
  );

  useHighlightTerms(searchFor, ".model-page-card-container", [isLoading], {
    exclude: ["th"],
  });

  const numPages = Math.ceil(data.totalResults / perPage);
  const firstShowing = (currentPage - 1) * perPage + 1;
  const lastShowing = firstShowing + data.page.length - 1;

  return (
    <div className="model-page">
      <p className="model-page-title">{props.title}</p>
      <DataControlBar
        filterHeader={props.filterHeader}
        selectedFilters={filters}
        onFilterOptionsChange={(filter, newSelected) => {
          // console.log(
          //   `${filter} option ${option} ${!newValue ? "de" : ""}selected`
          // );
          searchParams.delete(filter);
          for (let val of newSelected.values()) {
            searchParams.append(filter, val);
          }
          searchParams.delete("page");
          setSearchParams(searchParams);
        }}
        sortHeader={props.sortHeader}
        sortBy={sortBy}
        onSortByChange={(newSortBy) => {
          // console.log(`SortBy changed to ${newSortBy}`);
          searchParams.set("sort", newSortBy);
          searchParams.delete("page");
          setSearchParams(searchParams);
        }}
        searchFor={searchFor}
        onSearchChange={(newSearch) => {
          // console.log(`Search string changed to "${newSearch}"`);
          newSearch !== ""
            ? searchParams.set("search", newSearch)
            : searchParams.delete("search");
          searchParams.delete("page");
          setSearchParams(searchParams);
        }}
      />
      <Container className="model-page-card-container">
        {isLoading ? (
          <Row className="justify-content-center">
            <Spinner className="p-4 m-5" animation="border" />
          </Row>
        ) : (
          cloneElement(props.body, {
            data: data.page,
          })
        )}
      </Container>
      <Container style={{ padding: 0 }}>
        <div className="model-pagination">
          <DropdownButton
            title={`Results per page: ${perPage}`}
            variant=""
            drop="up"
            id="per-page-dropdown"
          >
            {[50, 40, 30, 20, 10].map((val) =>
              val != perPage ? (
                <Dropdown.Item
                  key={val}
                  onClick={() => {
                    if (val === props.resultsPerPage) {
                      searchParams.delete("perPage");
                    } else {
                      searchParams.set("perPage", val.toString());
                    }
                    setSearchParams(searchParams);
                  }}
                >
                  {val}
                </Dropdown.Item>
              ) : (
                ""
              )
            )}
          </DropdownButton>
          <PageControl
            activePage={currentPage}
            lastPage={numPages}
            updatePage={(p) => {
              searchParams.set("page", p.toString());
              setSearchParams(searchParams);
            }}
          />
          <p className="model-showing">
            Showing {firstShowing}-{lastShowing} of {data.totalResults}
          </p>
        </div>
      </Container>
    </div>
  );
};

ModelPage.defaultProps = {
  title: "Title",
  resultsPerPage: 20,
  filterHeader: {},
  sortHeader: {},
  body: "Body",
  dataSource: "",
};

export default ModelPage;
