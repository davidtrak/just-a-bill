// @flow

import React from "react";
import type { Node } from "react";
import { Pagination } from "react-bootstrap";
import "./PageControl.css";

type Props = {
  maxElements: number,
  activePage: number,
  firstPage: number,
  lastPage: number,
  updatePage: (...args: Array<number>) => any,
};

const PageControl = (props: Props): Node => {
  // How many on either side of active
  const span: number = Math.floor((props.maxElements - 4) / 2);

  const prevButtonKey = "prevButton";
  const nextButtonKey = "nextButton";
  const leftEllipsisKey = "leftEllipsis";
  const rightEllipsisKey = "rightEllipsis";

  // Layout used when not enough pages to use ellipses based on maxSpan
  const displayAll = () => {
    let items = [];
    for (let i = props.firstPage; i <= props.lastPage; i++) {
      items.push(
        <Pagination.Item
          key={i}
          active={props.activePage === i}
          onClick={() => {
            if (props.activePage !== i) props.updatePage(i);
          }}
        >
          {i}
        </Pagination.Item>
      );
    }
    return items;
  };

  // Layout used when active page is not near edge
  // Ex.  "1 ... 4 5 '6' 7 8 ... 10"
  const activeInMiddle = () => {
    let items = [];
    items.push(
      <Pagination.Item
        key={props.firstPage}
        onClick={() => {
          props.updatePage(props.firstPage);
        }}
      >
        {props.firstPage}
      </Pagination.Item>,
      <Pagination.Ellipsis key={leftEllipsisKey} />
    );
    for (let i = props.activePage - span; i <= props.activePage + span; i++) {
      items.push(
        <Pagination.Item
          key={i}
          active={props.activePage === i}
          onClick={() => {
            if (props.activePage !== i) props.updatePage(i);
          }}
        >
          {i}
        </Pagination.Item>
      );
    }
    items.push(
      <Pagination.Ellipsis key={rightEllipsisKey} />,
      <Pagination.Item
        key={props.lastPage}
        onClick={() => {
          props.updatePage(props.lastPage);
        }}
      >
        {props.lastPage}
      </Pagination.Item>
    );
    return items;
  };

  // Used when active is near edge
  // Ex.  "1 2 '3' ... 8 9 10"
  const activeOnEdge = () => {
    let items = [];
    for (let i = props.firstPage; i <= props.firstPage + span + 1; i++) {
      items.push(
        <Pagination.Item
          key={i}
          active={props.activePage === i}
          onClick={() => {
            if (props.activePage !== i) props.updatePage(i);
          }}
        >
          {i}
        </Pagination.Item>
      );
    }
    items.push(<Pagination.Ellipsis key={leftEllipsisKey} />);
    for (let i = props.lastPage - span - 1; i <= props.lastPage; i++) {
      items.push(
        <Pagination.Item
          key={i}
          active={props.activePage === i}
          onClick={() => {
            if (props.activePage !== i) props.updatePage(i);
          }}
        >
          {i}
        </Pagination.Item>
      );
    }
    return items;
  };

  let content;
  if (props.lastPage - props.firstPage + 1 <= props.maxElements) {
    content = displayAll();
  } else if (
    props.activePage > props.firstPage + span &&
    props.activePage < props.lastPage - span
  ) {
    content = activeInMiddle();
  } else {
    content = activeOnEdge();
  }

  return (
    <div className="page-control">
      <Pagination>
        <Pagination.Prev
          key={prevButtonKey}
          disabled={props.activePage === props.firstPage || props.firstPage === 0}
          onClick={() => {
            props.updatePage(props.activePage - 1);
          }}
        />
        {content}
        <Pagination.Next
          key={nextButtonKey}
          disabled={props.activePage === props.lastPage || props.lastPage === 0}
          onClick={() => {
            props.updatePage(props.activePage + 1);
          }}
        />
      </Pagination>
    </div>
  );
};

PageControl.defaultProps = {
  maxElements: 9,
  activePage: 1,
  firstPage: 1,
  lastPage: 10,
  updatePage: (p: number) => {
    console.log("Updating page to " + p);
  },
};

export default PageControl;
