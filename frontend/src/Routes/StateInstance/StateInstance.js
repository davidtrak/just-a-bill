import React from "react";
import { useParams } from "react-router-dom";
// import GoogleMapReact from "google-map-react";

import "./StateInstance.css";
import { Container, Row, Col, Card, ListGroup, Spinner } from "react-bootstrap";
import useInstanceData from "Common/Hooks/useInstanceData";
import CongressCard from "Common/ModelCards/CongressCard/CongressCard";
import BillCard from "Common/ModelCards/BillCard/BillCard";
import DisplayGrid from "Common/DisplayGrid/DisplayGrid";
import DisplayTable from "Common/DisplayTable/DisplayTable";
import useListData from "Common/Hooks/useListData";

const StateInstance = (props) => {
  let { id } = useParams();
  const [data, isLoading, isError] = useInstanceData("states", id);
  const [billsData, isBillsLoading, isBillsError] = useListData(
    "bills",
    1,
    30,
    `state_id=${id}`,
    "date_introduced"
  );
  const [congressData, isCongressLoading, isCongressError] = useListData(
    "congresspeople",
    1,
    55,
    `state_id=${id}`,
    "date_introduced"
  );

  // Data is null until done loading
  if (isLoading) {
    return (
      <Container>
        <Spinner className="p-4 m-5" animation="border" />
      </Container>
    );
  }

  const bgName =
    data.name.indexOf(" ") >= 1 ? data.name.replace(" ", "-") : data.name;

  return (
    <React.Fragment>
      <div
        className="state-inst"
        style={{
          backgroundImage:
            "url(https://cdn.civil.services/us-states/backgrounds/1920x1080/landscape/" +
            bgName.toLowerCase() +
            ".jpg)",
          backgroundSize: "cover",
        }}
      >
        <Container>
          <Row className="state-title">{data.name}</Row>
          <Row className="state-row-1">
            <Col className="state-image-col" xs={3}>
              <img
                className="state-image"
                src={`https://raw.githubusercontent.com/CivilServiceUSA/us-states/master/images/flags/${data.name.toLowerCase().replace(" ", "-")}-large.png`}
                alt="Unable to load"
              />
              <Row className="state-geninfo-title">
                <h3>General Information</h3>
              </Row>
              <Row className="state-geninfo">
                Population: {data.population} <br />
                Physical Size: {data.size} <br />
                Capital: {data.capital} <br />
                Largest City: {data.largestCity} <br />
                Party Affiliation (2020 pres. election): {data.party} <br />
                Number of Proposed Bills: {data.numProposedBills} <br />
              </Row>
            </Col>
            <Col className="state-image-col">
              <Row className="state-geninfo">
                <h3>Description</h3>
                <p>{data.description}</p>
              </Row>
              <Row className="state-geninfo">
                <h3>State Map</h3>
                <div className="google-map-code">
                  <iframe
                    src={`https://www.google.com/maps/embed/v1/place?key=${process.env.REACT_APP_GOOGLE_MAPS_API_KEY}&q=${data.name}`}
                    width="100%"
                    height="450"
                    frameBorder="0"
                    style={{ border: 0 }}
                    allowFullScreen=""
                    aria-hidden="false"
                    tabIndex="0"
                  ></iframe>
                </div>
              </Row>
            </Col>
          </Row>
          <Row className="mt-3">
            <Col className="state-congbill-card">
              <h3>Congress Members</h3>
              {isCongressLoading ? (
                <Spinner className="p-4 m-5" animation="border" />
              ) : (
                <DisplayGrid data={congressData.page} card={CongressCard} />
              )}
            </Col>
          </Row>
          <Row className="mt-3">
            <Col className="state-congbill-card">
              <h3>Bills Proposed</h3>
              {isBillsLoading ? (
                <Spinner className="p-4 m-5" animation="border" />
              ) : (
                <DisplayTable
                  pathToInstance="/bills/"
                  header={{
                    Bill: "number",
                    Title: "shortTitle",
                    Sponsor: "sponsor",
                    Committees: "committees",
                    "Introduced On": "introducedDate",
                  }}
                  data={billsData.page}
                  stateInst={true}
                />
              )}
            </Col>
          </Row>
        </Container>
        {/* </div> */}
      </div>
    </React.Fragment>
  );
};
export default StateInstance;
