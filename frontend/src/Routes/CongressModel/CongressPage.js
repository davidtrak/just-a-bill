// @flow

import React from "react";
import DisplayGrid from "Common/DisplayGrid/DisplayGrid";
import CongressCard from "Common/ModelCards/CongressCard/CongressCard";
import ModelPage from "Common/ModelPage/ModelPage";
import {
  congressFilterHeader,
  congressSortHeader,
} from "Library/FilterSortHeaders";

const CongressPage = (props: {}) => {
  return (
    <ModelPage
      title="Congresspeople"
      dataSource="congresspeople"
      filterHeader={congressFilterHeader}
      sortHeader={congressSortHeader}
      body={<DisplayGrid card={CongressCard} />}
    />
  );
};

export default CongressPage;
