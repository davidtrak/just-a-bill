import React from "react";

import backgroundImg from "Assets/Splash/splash-back.jpg";

import billIcon from "Assets/Common/Icons/bill_icon.png";
import stateIcon from "Assets/Common/Icons/state_icon.png";
import congresspersonIcon from "Assets/Common/Icons/congressperson_icon.png";

import SplashCard from "./SplashCard/SplashCard.js";
import { Container, Row, Col } from "react-bootstrap";
import "./Splash.css";

const Splash = () => {
  return (
    <>
      <div
        style={{
          backgroundImage: `url(${backgroundImg})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          height: "100vh",
          width: "100vw",
        }}
      >
        <Container fluid>
          <Row className="p-5 justify-content-center" xs="auto">
            <div className="title">justabill</div>
          </Row>
        </Container>
      </div>
      <Container fluid>
        <Col>
          <Col className="m-5 p-5">
            <div className="heading">It's time for you to get informed.</div>
          </Col>
          <Row className="justify-content-around p-5">
            <Col xs={10} sm={8} md={4} lg={3}>
              <SplashCard
                id={2}
                name={"States"}
                description={"Discover information about any U.S. State"}
                navPath={"/states"}
                img={stateIcon}
              />
            </Col>
            <Col xs={10} sm={8} md={4} lg={3}>
              <SplashCard
                id={1}
                name={"Bills"}
                description={
                  "Explore current or past bills and who's proposing them"
                }
                navPath={"/bills"}
                img={billIcon}
              />
            </Col>
            <Col xs={10} sm={8} md={4} lg={3}>
              <SplashCard
                id={2}
                name={"Congresspeople"}
                description={
                  "Meet your representatives and see what they're up to"
                }
                navPath={"/congresspeople"}
                img={congresspersonIcon}
              />
            </Col>
          </Row>
          <Row xs={12}>
            <div style={{ background: "dark" }} />
          </Row>
        </Col>
      </Container>
    </>
  );
};
export default Splash;
