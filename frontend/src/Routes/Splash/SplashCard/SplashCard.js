import React from "react";
import { Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

const SplashCard = (props) => {
  let navigate = useNavigate();

  let navToInstance = () => {
    console.log("Navigating to " + props.navPath);
    navigate(props.navPath);
  };

  return (
    <Card
      className="p-2 d-flex"
      border="dark"
      bg="dark"
      text="white"
      style={{ height: "100%", cursor: "pointer", borderRadius: "10px" }}
      onClick={() => {
        navToInstance();
      }}
    >
      <Card.Img className="p-5" variant="top" src={props.img} />
      <div className="mt-auto">
        <Card.Body>
          <Card.Title>{props.name}</Card.Title>
          <Card.Text>{props.description}</Card.Text>
        </Card.Body>
      </div>
    </Card>
  );
};

export default SplashCard;
