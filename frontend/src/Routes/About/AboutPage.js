import * as React from "react";
import { useEffect, useState } from "react";
import { Col, Row, Container } from "react-bootstrap";
import axios from "axios";
import BioCard from "./BioCard/BioCard";
import ToolCard from "./ToolCard/ToolCard";
import { bioData, toolData, apiData, ourData } from "Assets/About/aboutData";
import RepoCard from "./RepoCard/RepoCard";

const parseCommitData = (data) => {
  const commits = {};
  for (let commit of data) {
    const name = commit.committer_name.split(" ")[0];
    commits[name] = commits[name] + 1 || 1;
  }
  return commits;
};

const parseIssueData = (data) => {
  const issues = {};
  for (let key in data) {
    const issue = data[key];
    const author = issue.author.name.split(" ")[0];
    issues[author] = issues[author] + 1 || 1;
  }
  return issues;
};

const getAPIData = async (api: string) => {
  let page = 1;
  let lastData = [];
  let res = [];
  do {
    const nextPage = api + "&page=" + page;
    const response = await axios.get(nextPage);
    lastData = response.data;
    res = [...res, ...lastData];
    page++;
  } while (lastData.length !== 0);
  return res;
};

const AboutPage = (): React.Node => {
  const [commitData, setCommitData] = useState([]);
  const [issueData, setIssueData] = useState([]);

  const commitAPI =
    "https://gitlab.com/api/v4/projects/33965328/repository/commits?per_page=100&ref_name=develop";
  const issuesAPI =
    "https://gitlab.com/api/v4/projects/33965328/issues?per_page=100&all=true";

  useEffect(() => {
    getAPIData(commitAPI).then((res) => {
      setCommitData(res);
    });

    getAPIData(issuesAPI).then((res) => {
      setIssueData(res);
    });
  }, []);

  const commits = parseCommitData(commitData);
  const issues = parseIssueData(issueData);
  // TODO - compute this dynamically later
  const totalTests = bioData
    .map((d) => (d.unit_tests))
    .reduce((a, b) => {
      return a + b;
    });

  const data = bioData;
  for (let d of data) {
    const firstName = d.name.split(" ")[0];
    d.commits = commits[firstName]
      ? commits[firstName] + (firstName === "Connor" ? commits["P-Connor"] : 0)
      : -1;
    d.issues = issues[firstName] ?? -1;
  }

  return (
    <React.Fragment>
      <Container>
        <div className="aboutInfo">
          <h1>About Us</h1>
          <p>
            Just a bill was designed to help citizens become more informed
            voters. They can find their own representatives, see what bills they
            are working on, how they voted on previous bills, and learn more
            about the tendencies of their representatives. We hope to answer,
            through our project: Which bills have been proposed recently? Which
            state is producing the most bills and which states did in the past?
            Are congressmen/congresswomen involved with specific types of bills?
            Our application has an easy to use navigation bar which links to all
            models. Each model page lists multiple instances of each model and
            will later be able to be sorted and filtered. This easy to use
            interface will allow people to make the choice that they really
            want, and that can most benefit them and their communities.
          </p>

          <p>
            Finding information on proposed legislation can be difficult and
            time consuming; however, it doesn't have to be. By gathering data
            from disparate data sources of bills, congressional members, and
            states, we have assembled all of the data someone could ever need to
            learn more about legislation. In addition to data about the bills
            themselves, we have included information about the congressional
            members who proposed them, the states they are from, and more!
          </p>

          <h1>Team Members</h1>
          <Row>
            {data.map((personInfo, index) => {
              return (
                <Col key={index}>
                  <BioCard {...personInfo} />
                </Col>
              );
            })}
          </Row>

          <h3>Repository Totals</h3>
          <Row>
            <Col>
              <RepoCard
                title="Commits"
                num={Object.values(commits).reduce((a, b) => a + b, 0)}
              ></RepoCard>
            </Col>
            <Col>
              <RepoCard
                title="Issues"
                num={Object.values(issues).reduce((a, b) => a + b, 0)}
              ></RepoCard>
            </Col>
            <Col>
              <RepoCard title="Unit tests" num={totalTests}></RepoCard>
            </Col>
          </Row>

          <h3>Tools Utilized</h3>
          <Row md={4} lg={6} className={"justify-content-md-center"}>
            {toolData.map((toolInfo, index) => {
              return (
                <Col key={index}>
                  <ToolCard {...toolInfo} />
                </Col>
              );
            })}
          </Row>

          <h3>Data Sources</h3>
          <Row md={4} lg={6} className={"justify-content-md-center"}>
            {apiData.map((apiInfo, index) => {
              return (
                <Col key={index}>
                  <ToolCard {...apiInfo} />
                </Col>
              );
            })}
          </Row>

          <h3>Other Info</h3>
          <Row md={4} lg={6} className={"justify-content-md-center"}>
            {ourData.map((ourInfo, index) => {
              return (
                <Col key={index}>
                  <ToolCard {...ourInfo} />
                </Col>
              );
            })}
          </Row>
        </div>
      </Container>
    </React.Fragment>
  );
};
export default AboutPage;
