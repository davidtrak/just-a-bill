// @flow

import * as React from "react";
import { Card, ListGroup } from "react-bootstrap";
import "../About.css";

type BioCardProps = {
  name: string,
  role: string,
  leadership_role: string,
  bio: string,
  image: string,
  commits: number,
  issues: number,
  unit_tests: number,
};

const BioCard = (props: BioCardProps): React.Node => {
  const {
    name,
    role,
    leadership_role,
    bio,
    image,
    commits,
    issues,
    unit_tests,
  } = props;

  return (
    <Card className="bio-card">
      <Card.Img variant="top" src={image} />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle className="margin-bottom: 15px">{role}</Card.Subtitle>
        <Card.Subtitle className="margin-bottom: 15px">
          {leadership_role}
        </Card.Subtitle>
        <Card.Text>{bio}</Card.Text>
      </Card.Body>
      <Card.Body className="d-flex align-items-end justify-content-center">
        <ListGroup>
          <ListGroup.Item>
            <strong>{commits === -1 ? "--" : commits}</strong> commits
          </ListGroup.Item>
          <ListGroup.Item>
            <strong>{issues === -1 ? "--" : issues}</strong> issues
          </ListGroup.Item>
          <ListGroup.Item>
            <strong>{unit_tests}</strong> unit tests
          </ListGroup.Item>
        </ListGroup>
      </Card.Body>
    </Card>
  );
};

export default BioCard;
