import React from "react";
import { shallow } from "enzyme";
import BioCard from "./BioCard";

const defaultProps = {
  name: "Ricky Woodruff",
  role: "Full Stack Engineer",
  bio: "testbio",
  image: "./headshots/ricky.jpg",
  commits: 10,
  issues: 5,
  unit_tests: 4,
};

test("renders expected bio data", () => {
  const wrapper = shallow(<BioCard {...defaultProps} />);

  expect(wrapper.find("CardImg").prop("src")).toEqual("./headshots/ricky.jpg");
  expect(wrapper.text()).toContain("Ricky Woodruff");
  expect(wrapper.text()).toContain("Full Stack Engineer");
  expect(wrapper.text()).toContain("testbio");
  expect(wrapper.text()).toContain("10 commits");
  expect(wrapper.text()).toContain("5 issues");
  expect(wrapper.text()).toContain("4 unit tests");
});
