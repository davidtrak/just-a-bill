import React from "react";
import { shallow } from "enzyme";
import ToolCard from "Routes/About/ToolCard/ToolCard";

const defaultProps = {
  url: "https://google.com",
  img: "test.jpg",
  title: "MockTitle",
  description: "test description",
};
it("renders expected data", () => {
  const wrapper = shallow(<ToolCard {...defaultProps} />);

  expect(wrapper.find("a").prop("href")).toEqual(defaultProps.url);
  expect(wrapper.find("CardImg").prop("src")).toEqual(defaultProps.img);
  expect(wrapper.find("CardTitle").text()).toEqual(defaultProps.title);
  expect(wrapper.find("CardText").text()).toEqual(defaultProps.description);
});
