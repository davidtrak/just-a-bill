// @flow

import * as React from "react";
import { Card } from "react-bootstrap";

type ToolCardProps = {
  url: string,
  img: string,
  title: string,
  description: string,
};

const ToolCard = (props: ToolCardProps): React.Node => {
  const { url, img, title, description } = props;

  return (
    <a href={url}>
      <Card className="tool-card">
        <Card.Img className="mt-auto" variant="top" src={img} />
        <div className="mt-auto">
          <Card.Body>
            <Card.Title>{title}</Card.Title>
            <Card.Text>{description}</Card.Text>
          </Card.Body>
        </div>
      </Card>
    </a>
  );
};

export default ToolCard;
