// @flow

import * as React from "react";
import { Card } from "react-bootstrap";

type RepoCardProps = {
  title: string,
  num: number,
};

const RepoCard = (props: RepoCardProps): React.Node => {
  const { title, num } = props;

  return (
    <Card className="repo-card">
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Text>{num}</Card.Text>
      </Card.Body>
    </Card>
  );
};

export default RepoCard;
