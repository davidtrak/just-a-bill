import { shallow } from "enzyme";
import React from "react";
import RepoCard from "Routes/About/RepoCard/RepoCard";

const defaultProps = {
  title: "RepoTitle",
  num: 4,
};
it("renders expected data", () => {
  const wrapper = shallow(<RepoCard {...defaultProps} />);

  expect(wrapper.find("CardTitle").text()).toEqual(defaultProps.title);
  expect(wrapper.find("CardText").text()).toEqual("4");
});
