import React from "react";
import { shallow } from "enzyme";
import AboutPage from "./AboutPage";

test("renders important About info", () => {
  const wrapper = shallow(<AboutPage />);

  expect(wrapper.find("h1").first().text()).toEqual("About Us");

  expect(wrapper.find("p").first().text()).toContain(
    "Just a bill was designed"
  );

  expect(wrapper.find("p").at(1).text()).toContain(
    "Finding information on proposed legislation"
  );

  expect(wrapper.find("h3").at(0).text()).toEqual("Repository Totals");
  expect(wrapper.find("RepoCard").at(0).prop("title")).toEqual("Commits");
  expect(wrapper.find("RepoCard").at(1).prop("title")).toEqual("Issues");
  expect(wrapper.find("RepoCard").at(2).prop("title")).toEqual("Unit tests");

  expect(wrapper.find("h3").at(1).text()).toEqual("Tools Utilized");
  expect(wrapper.find("h3").at(2).text()).toEqual("Data Sources");
  expect(wrapper.find("h3").at(3).text()).toEqual("Other Info");

  expect(wrapper.find("h1").at(1).text()).toEqual("Team Members");

  expect(wrapper.find("BioCard").length).toEqual(5);
});
