import React from "react";
import DisplayTable from "Common/DisplayTable/DisplayTable";
import ModelPage from "Common/ModelPage/ModelPage";
import { billFilterHeader, billSortHeader } from "Library/FilterSortHeaders";

const BillsPage = (props) => {
  return (
    <ModelPage
      title="Bills"
      dataSource="bills"
      filterHeader={billFilterHeader}
      sortHeader={billSortHeader}
      body={
        <DisplayTable
          pathToInstance="/bills/"
          header={{
            Bill: "number",
            Title: "shortTitle",
            Sponsor: "sponsor",
            Committees: "committees",
            "Introduced On": "introducedDate",
          }}
        />
      }
    />
  );
};

export default BillsPage;
