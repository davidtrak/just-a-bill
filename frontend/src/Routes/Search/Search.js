// @flow

import React, { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import { fetchListData } from "../../Library/Data.js";
import StateCard from "Common/ModelCards/StateCard/StateCard.js";
import DisplayTable from "Common/DisplayTable/DisplayTable.js";
import DisplayGrid from "Common/DisplayGrid/DisplayGrid.js";
import CongressCard from "Common/ModelCards/CongressCard/CongressCard.js";
import SearchBar from "Common/SearchBar/SearchBar.js";
import { Tabs, Tab, Container, Spinner, Row, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import useHighlightTerms from "Common/Hooks/useHighlightTerms";

import "./Search.css";
import "../../Common/ModelPage/ModelPage.css";
import "../../Common/DataControlBar/DataControlBar.css";

const Search = () => {
  const navigate = useNavigate();

  const [searchParams, setSearchParams] = useSearchParams({ search: "" });
  const searchQuery = searchParams.get("search") ?? "";

  const [searchResults, setSearchResults] = useState(null);
  const [loading, setLoading] = useState(false);

  const perPage = 5;

  useEffect(() => {
    if (searchQuery !== "") {
      console.log("loading...");
      setLoading(true);
      fetchSearchResults();
    }
  }, [searchQuery]);

  useHighlightTerms(searchQuery, ".search-container", [loading], {
    exclude: ["h3", "button"],
  });

  const fetchSearchResults = async () => {
    // to pass by name need to change parameters in fLD({} = {}), doing this for now
    let results = {
      states: await fetchListData("states", 1, perPage, "", "", searchQuery),
      bills: await fetchListData("bills", 1, perPage, "", "", searchQuery),
      congresspeople: await fetchListData(
        "congresspeople",
        1,
        perPage,
        "",
        "",
        searchQuery
      ),
    };
    setSearchResults(results);
    setLoading(false);
  };

  const getMoreResultsButton = (number: number, navigateTo: string) => {
    if (number > 0) {
      return (
        <Row className="justify-content-end">
          <Button
            variant="outline-secondary"
            style={{ width: "auto", marginRight: "1em" }}
            onClick={() => {
              navigate(navigateTo);
            }}
          >
            View {number} more result{number > 1 ? "s" : ""} >
          </Button>
        </Row>
      );
    }
    return <></>;
  };

  const getBillResults = () => {
    if (searchResults !== null && searchResults.bills.totalResults !== 0) {
      return (
        <Container className="search-container">
          <h3>Bills Results</h3>

          <DisplayTable
            pathToInstance="/bills/"
            header={{
              Bill: "number",
              Title: "shortTitle",
              Sponsor: "sponsor",
              Committees: "committees",
              "Introduced On": "introducedDate",
            }}
            highlightString={searchQuery}
            data={searchResults.bills.page}
          />
          {getMoreResultsButton(
            searchResults.bills.totalResults - perPage,
            `/bills?search=${searchQuery}`
          )}
        </Container>
      );
    }
  };

  const getCongresspeopleResults = () => {
    if (
      searchResults !== null &&
      searchResults.congresspeople.totalResults !== 0
    ) {
      return (
        <Container className="search-container">
          <h3>Congresspeople Results</h3>
          <DisplayGrid
            data={searchResults.congresspeople.page}
            card={CongressCard}
            highlightString={searchQuery}
          />
          {getMoreResultsButton(
            searchResults.congresspeople.totalResults - perPage,
            `/congresspeople?search=${searchQuery}`
          )}
        </Container>
      );
    }
  };

  const getStatesResults = () => {
    if (searchResults !== null && searchResults.states.totalResults !== 0) {
      return (
        <Container className="search-container">
          <h3>States Results</h3>
          <DisplayGrid
            data={searchResults.states.page}
            card={StateCard}
            highlightString={searchQuery}
          />
          {getMoreResultsButton(
            searchResults.states.totalResults - perPage,
            `/states?search=${searchQuery}`
          )}
        </Container>
      );
    }
  };

  const getContent = () => {
    if (searchQuery === "") {
      return <> </>;
    } else if (loading || searchResults === null) {
      return (
        <Container className="search-container">
          <Row className="justify-content-center">
            <Spinner className="p-4 m-5" animation="border" />
          </Row>
        </Container>
      );
    } else {
      let arr: string[] = [];
      if (searchResults.bills.totalResults === 0) {
        arr.push("bills");
      }
      if (searchResults.congresspeople.totalResults === 0) {
        arr.push("congresspeople");
      }
      if (searchResults.states.totalResults === 0) {
        arr.push("states");
      }
      let noResultsFor = "";
      if (arr.length > 0) {
        let last = arr.pop();
        noResultsFor = arr.length > 0 ? arr.join(", ") + " or " + last : last;
        arr.push(last);
      }

      return (
        <>
          {getBillResults()}
          {getCongresspeopleResults()}
          {getStatesResults()}
          {noResultsFor !== "" ? (
            <Container className="search-container p-4">
              <h5>No {noResultsFor} found for "{searchQuery}"</h5>
            </Container>
          ) : (
            ""
          )}
        </>
      );
    }
  };

  return (
    <>
      <p className="model-page-title">Search</p>
      <Container className="data-control-bar">
        <SearchBar
          placeholderText="Search for States, Congresspeople, or Bills"
          initialValue={searchQuery}
          onSubmit={(newSearch) => {
            if (newSearch !== "") {
              setSearchParams({ search: newSearch });
            } else {
              setSearchParams({});
            }
          }}
        />
      </Container>
      {getContent()}
    </>
  );
};

export default Search;
