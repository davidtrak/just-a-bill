import React from "react";
import DisplayGrid from "Common/DisplayGrid/DisplayGrid";
import StateCard from "Common/ModelCards/StateCard/StateCard";
import ModelPage from "Common/ModelPage/ModelPage";
import { stateFilterHeader, stateSortHeader } from "Library/FilterSortHeaders";

const StatePage = (props) => {
  return (
    <ModelPage
      title="States"
      dataSource="states"
      filterHeader={stateFilterHeader}
      sortHeader={stateSortHeader}
      body={<DisplayGrid card={StateCard} />}
    />
  );
};

export default StatePage;
