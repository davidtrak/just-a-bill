// @flow

import * as React from "react";
import axios from "axios";
import { useEffect, useState } from "react";
import { Container, Row, Col, Card, ListGroup, Spinner } from "react-bootstrap";
import useInstanceData from "Common/Hooks/useInstanceData";
import useListData from "Common/Hooks/useListData";
import DisplayTable from "Common/DisplayTable/DisplayTable";
import { Link, useParams } from "react-router-dom";
import { Timeline } from "react-twitter-widgets";
import { PieChart } from "react-minimal-pie-chart";
import { CongresspersonModel } from "Library/DataModels";

import "./CongressInstance.css";

const CongressInstance = (props: {}): React.Node => {
  let { id } = useParams();
  const backgroundImg =
    "url(https://cdn.britannica.com/66/164166-050-4FBB1C5A/Chamber-US-House-of-Representatives-Washington-DC.jpg)";
  const [data, isLoading, isError] = useInstanceData("congresspeople", id);
  const [billsData, isBillsLoading, isBillsError] = useListData(
    "bills",
    1,
    30,
    `congressperson_id=${id}`,
    "date_introduced"
  );

  // This is just some jank until we get descriptions/wikipedia links in the api
  const [description, setDescription] = useState("");
  useEffect(() => {
    if (!isLoading && data.wikipediaUrl) {
      let url = `https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=${data.wikipediaUrl
        .split("/")
        .at(-1)}&origin=*`;
      console.log(`Getting description from ${url}`);
      axios
        .get(url)
        .then((response) => {
          setDescription(Object.values(response.data.query.pages)[0].extract);
        })
        .catch((error) => {});
    }
  }, [data, isLoading]);

  if (isLoading) {
    return (
      <Container>
        <Spinner className="p-4 m-5" animation="border" />
      </Container>
    );
  }

  if (isError || !(data instanceof CongresspersonModel)) {
    return (
      <Container style={{ textAlign: "center", padding: "2rem" }}>
        <p>An error has occurred</p>
      </Container>
    );
  }

  const name = () => {
    return (
      <div className="cong-name" style={{ flex: "1 0 100%" }}>
        <h1>{data.fullName}</h1>
        <h5>
          {!data.inOffice ? "Former " : ""}
          {data.leadershipRole !== null ? data.leadershipRole : data.title}
        </h5>
      </div>
    );
  };

  const genInfo = () => {
    return (
      <div className="cong-box" style={{ flex: "0 1 20rem" }}>
        <img
          style={{ width: "100%" }}
          src={data.headshotUrl}
          alt="Unable to load"
        />

        <h3 className="cong-geninfo-title">General Information</h3>
        <p>
          {data.party} <br />
          Seniority: {data.seniority} <br />
          Age: {data.age} <br />
          Gender: {data.gender} <br />
          State:{" "}
          <Link to={"/states/" + data.stateId} className="cong-link">
            {data.state}
          </Link>{" "}
          {data.district ? `(District ${data.district})` : ""}
        </p>
      </div>
    );
  };

  const contactBox = () => {
    let header = {
      Phone: data.phone,
      Office: data.office,
      Fax: data.fax,
      "Contact Form": data.contactForm,
    };

    return (
      <div className="cong-box" style={{ flex: "1 0 100%" }}>
        <h3>Contact</h3>
        <div className="cong-textbox-row">
          {Object.entries(header).map(([key, val]) =>
            val !== null ? (
              <p className="cong-textbox">
                {`${key}: `}
                {val.includes("http") || val.includes("www.") ? (
                  <a className="cong-link" href={val}>
                    {val.toString()}
                  </a>
                ) : (
                  val.toString()
                )}
              </p>
            ) : (
              ""
            )
          )}
        </div>
      </div>
    );
  };

  const descriptionBox = () => {
    if (description !== "") {
      return (
        <div className="cong-box">
          <h3>Description</h3>
          <p>{description}</p>
        </div>
      );
    }
  };

  const mediaBox = () => {
    return (
      <div className="cong-box" style={{ flex: "1 0 100%" }}>
        <h3>Media</h3>
        <div className="cong-textbox-row">
          {data.website === null &&
          data.twitterAccount === null &&
          data.facebookAccount === null &&
          data.youtubeAccount === null ? (
            <p className="cong-textbox">No media</p>
          ) : (
            ""
          )}
          {data.website !== null ? (
            <p className="cong-textbox">
              Website:
              <a className="cong-link" href={data.website}>
                {" " + data.website}
              </a>
            </p>
          ) : (
            ""
          )}
          {data.facebookAccount !== null ? (
            <p className="cong-textbox">
              Facebook:
              <a
                className="cong-link"
                href={"https://www.facebook.com/" + data.facebookAccount}
              >
                {" " + data.facebookAccount}
              </a>
            </p>
          ) : (
            ""
          )}
          {data.twitterAccount !== null ? (
            <p className="cong-textbox">
              Twitter:
              <a
                className="cong-link"
                href={"https://twitter.com/" + data.facebookAccount}
              >
                {" " + data.twitterAccount}
              </a>
            </p>
          ) : (
            ""
          )}
          {data.youtubeAccount !== null ? (
            <p className="cong-textbox">
              Youtube:
              <a
                className="cong-link"
                href={"https://www.youtube.com/user/" + data.youtubeAccount}
              >
                {" " + data.youtubeAccount}
              </a>
            </p>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  };

  const timeline = () => {
    return (
      <div style={{ flex: "3 0 25rem" }}>
        <Timeline
          dataSource={{
            sourceType: "profile",
            screenName: data.twitterAccount,
          }}
          options={{
            theme: "dark",
            height: "500",
          }}
        />
      </div>
    );
  };

  const votesBox = () => {
    let total = data.totalVotes;
    let voted = data.totalVotes - data.missedVotes;
    let againstParty = Math.round((voted * data.votesAgainstPartyPct) / 100);
    let withParty = voted - againstParty;

    return (
      <div className="cong-box" style={{ flex: "1 0 100%" }}>
        <div className="cong-row">
          <div style={{ flex: "1 0 100%" }}>
            <h3>Voting History</h3>{" "}
          </div>
          <div style={{ flex: "1 0 20rem", maxHeight: "18rem" }}>
            <PieChart
              label={({ dataEntry }) =>
                Math.round(dataEntry.percentage * 10) / 10 + "%"
              }
              labelStyle={{
                fill: "white",
                fontSize: "0.3rem",
                margin: "1em",
              }}
              radius={38}
              labelPosition={120}
              data={[
                {
                  title: "Votes with party",
                  value: withParty,
                  color: "lightgreen",
                },
                {
                  title: "Votes against party",
                  value: againstParty,
                  color: "tomato",
                },
                {
                  title: "Missed",
                  value: data.missedVotes,
                  color: "#FFA36C",
                },
              ]}
            />
          </div>
          <div style={{ flex: "1 0 10em" }}>
            {" "}
            <h4>117th congress</h4>
            <hr />
            <p>Total votes: {data.totalVotes}</p>
            <p>Votes with party: {withParty}</p>
            <p>Votes against party: {againstParty}</p>
            <p>Missed votes: {data.missedVotes}</p>
          </div>
        </div>
      </div>
    );
  };

  const billsBox = () => {
    let content = "";
    if (!isBillsLoading) {
      if (billsData.totalResults > 0) {
        content = (
          <DisplayTable
            pathToInstance="/bills/"
            header={{
              Bill: "number",
              Title: "shortTitle",
              Sponsor: "sponsor",
              Committees: "committees",
              "Introduced On": "introducedDate",
            }}
            data={billsData.page}
            stateInst={true}
          />
        );
      } else {
        content = <p>We don't have any bills sponsored by {data.fullName}</p>;
      }
    } else {
      content = <Spinner className="p-4 m-5" animation="border" />;
    }
    return (
      <div className="cong-box" style={{ fontSize: "1rem", flex: "1 1 100%" }}>
        <h3>Bills Sponsored</h3>
        {content}
      </div>
    );
  };

  return (
    <div
      className="cong-inst"
      style={{
        backgroundImage: backgroundImg,
      }}
    >
      <Container>
        <div className="cong-row">
          {name()}
          {genInfo()}
          <div className="cong-row" style={{ flex: "1 1 30rem" }}>
            {descriptionBox()}
            <div className="cong-row" style={{ flex: "1 1 20rem" }}>
              {mediaBox()}
              {contactBox()}
            </div>
            {timeline()}
          </div>
          {votesBox()}
          {billsBox()}
        </div>
      </Container>
    </div>
  );
};
export default CongressInstance;
