import React from "react";
import { Tabs, Tab } from "react-bootstrap";
import BillPieChart from "./OurVisualizations/BillPieChart";
import StateBubbleGraph from "./OurVisualizations/StateBubbleGraph";
import TermFunnel from "./OurVisualizations/TermFunnel";
import TravelBarGraph from "./SafeTravelsVisualizations/TravelBarGraph";
import PopularAirports from "./SafeTravelsVisualizations/PopularAirports";
import CovidRisk from "./SafeTravelsVisualizations/CovidRisk";

const VisualizationPage = () => {
  return (
    <React.Fragment>
      <p className="model-page-title">Visualizations</p>
      <Tabs
        defaultActiveKey="ours"
        id="uncontrolled-tab-example"
        className="mb-3"
      >
        <Tab eventKey="ours" title="Ours">
          <StateBubbleGraph />
          <BillPieChart />
          <TermFunnel />
        </Tab>
        <Tab eventKey="safetravels" title="Safe Travels">
          <TravelBarGraph />
          <PopularAirports />
          <CovidRisk />
        </Tab>
      </Tabs>
    </React.Fragment>
  );
};

export default VisualizationPage;
