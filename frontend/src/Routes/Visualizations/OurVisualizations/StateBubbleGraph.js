import React, { useEffect, useState } from "react";
import BubbleChart from "@weknow/react-bubble-chart-d3";
import useListData from "Common/Hooks/useListData";
import { Container, Spinner, Tab, Row } from "react-bootstrap";
import { Tabs } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";

const StateBubbleGraph = () => {
  const [data, isLoading, isError] = useListData(
    "congresspeople",
    1,
    543,
    "",
    "",
    ""
  );
  const [stateMap] = useState(new Map());
  const [loyalData, setLoyalData] = useState(null);
  const [disloyalData, setDisloyalData] = useState(null);

  let navigate = useNavigate();

  let navToInstance = (path) => {
    navigate(path);
  };

  useEffect(() => {
    if (!isLoading) {
      let dict = {};
      data.page.forEach((person) => {
        if (person.state in dict) {
          if (person.votesAgainstPartyPct !== null) {
            dict[person.state].against += person.votesAgainstPartyPct;
            dict[person.state].with += person.votesWithPartyPct;
            dict[person.state].numCong += 1;
          }
        } else {
          dict[person.state] = {
            against: person.votesAgainstPartyPct,
            with: person.votesWithPartyPct,
            numCong: 1,
          };
        }
        if (!(person.state in stateMap)) {
          stateMap.set(person.state, person.stateId);
        }
      });
      const withData = [];
      const againstData = [];
      for (const [key, value] of Object.entries(dict)) {
        dict[key].against /= dict[key].numCong;
        dict[key].with /= dict[key].numCong;
        withData.push({
          label: key,
          value: Math.round(dict[key].with),
        });
        againstData.push({
          label: key,
          value: Math.round(dict[key].against),
        });
      }
      setLoyalData(withData);
      setDisloyalData(againstData);
    }
  }, [data, isLoading]);

  if (isLoading) {
    return (
      <Container>
        <Row className="justify-content-center">
          <Spinner className="p-4 m-5" animation="border" />
        </Row>
      </Container>
    );
  }

  /*

  ADD AN ON CLICK FUNC TO TAKE TO STATE!!!!


  */
  return (
    <React.Fragment>
      <Container className="data-control-bar">
        <h3>States by Congresspeople's loyalty to their Party</h3>
        <Tabs
          defaultActiveKey="disloyalty"
          id="uncontrolled-tab-example"
          className="mb-3"
        >
          <Tab eventKey="disloyalty" title="Disloyal">
            <h4>
              Number represents the average percentage of votes casted by
              congresspeople which were AGAINST their party
            </h4>
            <h5>Alternative Title: States by Disloyal congresspeople</h5>
            <div className="bubble-chart">
              <BubbleChart
                graph={{ zoom: 0.7 }}
                padding={1}
                height={700}
                showLegend={false}
                data={disloyalData}
                valueFont={{
                  family: "Arial",
                  size: 20,
                  color: "#fff",
                  weight: "bold",
                }}
                labelFont={{
                  family: "Arial",
                  size: 20,
                  color: "#fff",
                  weight: "bold",
                }}
                bubbleClickFun={(val) =>
                  navToInstance("/states/" + stateMap.get(val))
                }
              />
            </div>
          </Tab>
          <Tab eventKey="loyalty" title="Loyal">
            <h4>
              Number represents the average percentage of votes casted by
              congresspeople which were WITH their party
            </h4>
            <h5>Alternative Title: States by Loyal congresspeople</h5>
            <div className="bubble-chart">
              <BubbleChart
                graph={{ zoom: 0.7 }}
                padding={1}
                height={700}
                showLegend={false}
                data={loyalData}
                valueFont={{
                  family: "Arial",
                  size: 12,
                  color: "#fff",
                  weight: "bold",
                }}
                labelFont={{
                  family: "Arial",
                  size: 14,
                  color: "#fff",
                  weight: "bold",
                }}
                bubbleClickFun={(val) =>
                  navToInstance("/states/" + stateMap.get(val))
                }
              />
            </div>
          </Tab>
        </Tabs>
      </Container>
    </React.Fragment>
  );
};

export default StateBubbleGraph;
