import React, { useEffect, useState } from "react";
import useListData from "Common/Hooks/useListData";
import {
  FunnelChart,
  Tooltip,
  ResponsiveContainer,
  Funnel,
  LabelList,
} from "recharts";
import { Spinner, Container, Col, Row } from "react-bootstrap";
import "./TermFunnel.css";

const COLORS = [
  "#0088FE",
  "#00C49F",
  "#FFBB28",
  "#FF8042",
  "#440BD4",
  "#FF2079",
];

const groupData = (groups, currentValue, currentIndex) => {
  const group = Math.floor(currentValue / 4);
  for (let i = 0; i <= Math.min(5, group); i++) {
    groups[i] = groups[i] ?? { value: 0 };
    groups[i]["value"] += 1;
  }

  return groups;
};

const parseData = (data) => {
  let res = data.page.map((cp) => cp.seniority).reduce(groupData, {});
  const parsedData = Object.entries(res).map(([key, value], index) => {
    const intKey = parseInt(key);
    const lowerbound = intKey * 4;
    const upperbound = (intKey + 1) * 4 - 1;
    return {
      name:
        lowerbound == 20 ? "20+ years" : `${lowerbound} - ${upperbound} years`,
      value: value["value"],
      fill: COLORS[index],
    };
  });
  return parsedData;
};

const TermFunnel = () => {
  const [data, isLoading, isError] = useListData("congresspeople", 1, 550);
  const [displayData, setDisplayData] = useState([]);

  if (isLoading) {
    return (
      <Container>
        <Row className="justify-content-center">
          <Spinner className="p-4 m-5" animation="border" />
        </Row>
      </Container>
    );
  } else if (isError) {
    return <React.Fragment />;
  }

  const parsedData = parseData(data);

  return (
    <Container id="term-funnel">
      <Col>
        <h3>Tenure in Congress</h3>
        <FunnelChart width={600} height={400}>
          <Tooltip />
          <Funnel dataKey="value" data={parsedData} isAnimationActive>
            <LabelList
              position="right"
              fill="#000"
              stroke="none"
              dataKey="name"
              offset="10"
            />
          </Funnel>
        </FunnelChart>
      </Col>
    </Container>
  );
};

export default TermFunnel;
