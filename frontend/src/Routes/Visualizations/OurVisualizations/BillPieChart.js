import useListData from "Common/Hooks/useListData";
import React, { useEffect, useState } from "react";
import {
  PieChart,
  Pie,
  Sector,
  Cell,
  ResponsiveContainer,
  Tooltip,
  Legend,
} from "recharts";
import { Spinner, Container, Row } from "react-bootstrap";
import "../Visualizations.css";

const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];

const BillPieChart = () => {
  const [data, isLoading, isError] = useListData("bills", 1, 500, "", "", "");
  const [displayData, setDisplayData] = useState(null);

  useEffect(() => {
    if (!isLoading) {
      const values = [];
      data.page.forEach((bill) => {
        if (bill.primarySubject !== "") {
          if (bill.primarySubject in values) {
            values[bill.primarySubject] += 1;
          } else {
            values[bill.primarySubject] = 1;
          }
        }
      });
      const temp = [];
      for (const [key, value] of Object.entries(values)) {
        temp.push({
          name: key,
          value: value,
        });
      }
      setDisplayData(temp);
    }
  }, [data, isLoading]);

  if (isLoading) {
    return (
      <Container>
        <Row className="justify-content-center">
          <Spinner className="p-4 m-5" animation="border" />
        </Row>
      </Container>
    );
  }

  const CustomTooltip = ({ active, payload, label }) => {
    if (active && payload && payload.length) {
      return (
        <div className="custom-tooltip">
          <p className="label">{`${payload[0].name} : ${Math.round(
            (payload[0].value / 282) * 100
          )}%`}</p>
        </div>
      );
    }

    return null;
  };

  return (
    <React.Fragment>
      <Container className="data-control-bar">
        <h3>Proposed Bills by Primary Subject</h3>
        <PieChart width={1000} height={700}>
          <Pie
            data={displayData}
            cx="50%"
            cy="50%"
            outerRadius={210}
            fill="#8884d8"
            dataKey="value"
            nameKey="name"
            legendType="line"
          >
            {displayData == null
              ? ""
              : displayData.map((entry, index) => (
                  <Cell
                    key={`cell-${index}`}
                    fill={COLORS[index % COLORS.length]}
                  />
                ))}
          </Pie>
          <Legend layout="vertical" align="right" />
          <Tooltip content={<CustomTooltip />} />
        </PieChart>
      </Container>
    </React.Fragment>
  );
};

export default BillPieChart;
