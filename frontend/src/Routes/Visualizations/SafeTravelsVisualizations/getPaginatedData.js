
import axios from "axios";

 const getPaginatedData = async (api: string) => {
    let page = 1;
    let lastData = [];
    let res = [];
    do {
      const nextPage = api + "&page=" + page;
      const response = await axios.get(nextPage);
      lastData = response.data;
      page = response.data.next;
      res = [...res, ...lastData.data];
    } while (page !== null);
    return res;
  };

  export default getPaginatedData;