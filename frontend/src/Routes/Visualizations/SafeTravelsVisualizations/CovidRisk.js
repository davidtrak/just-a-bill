import React, { useEffect, useState, useRef } from "react";
import {
  PieChart,
  Pie,
  Sector,
  Cell,
  ResponsiveContainer,
  Tooltip,
  Legend,
  Label,
} from "recharts";
import getPaginatedData from "./getPaginatedData";
import { Spinner, Container, Row } from "react-bootstrap";
import "../Visualizations.css";

const COLORS = {
  Medium: "#FFBB28",
  High: "#FF8042",
  Extreme: "#440BD4",
};

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  name,
  percent,
}) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text
      x={x}
      y={y}
      fill="white"
      textAnchor={x > cx ? "start" : "end"}
      dominantBaseline="central"
    >
      {`${name}: ${(percent * 100).toFixed(0)}%`}
    </text>
  );
};

const CovidRisk = () => {
  const [data, setData] = useState(null);
  const risks = useRef({}); // risk to total number of cities at that risk

  useEffect(() => {
    const api = "https://www.api.plansafetrips.me/cities?page_size=20";
    getPaginatedData(api).then((res) => {
      setData(res);
    });
  }, []);

  if (data === null) {
    return (
      <Container>
        <Row className="justify-content-center">
          <Spinner className="p-4 m-5" animation="border" />
        </Row>
      </Container>
    );
  }

  risks.current = {};
  data.forEach((city) => {
    if (!risks.current[city.risk_level]) {
      risks.current[city.risk_level] = 0;
    }
    risks.current[city.risk_level] += 1;
  });
  const displayData = Object.entries(risks.current).map(
    ([riskLevel, total]) => ({ name: riskLevel, value: total })
  );

  return (
    <React.Fragment>
      <Container className="data-control-bar">
        <h3>Number of Cities by COVID Risk Level:</h3>
        <PieChart width={1000} height={700}>
          <Pie
            data={displayData}
            cx="50%"
            cy="50%"
            outerRadius={210}
            fill="#8884d8"
            dataKey="value"
            nameKey="name"
            label={renderCustomizedLabel}
            labelLine={false}
          >
            {displayData.map((entry, index) => (
              <Cell key={`cell-${index}`} fill={COLORS[entry.name]} />
            ))}
          </Pie>
          <Tooltip
            content={({ active, payload, label }) => {
              if (active && payload && payload.length) {
                return (
                  <div className="custom-tooltip">
                    <p className="label">{`${payload[0].value} cities`}</p>
                  </div>
                );
              }

              return null;
            }}
          />
        </PieChart>
      </Container>
    </React.Fragment>
  );
};

export default CovidRisk;
