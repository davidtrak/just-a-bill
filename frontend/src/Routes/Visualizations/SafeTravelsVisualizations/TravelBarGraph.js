import React, { useEffect, useState } from "react";
import {
  BarChart,
  Tooltip,
  Legend,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
} from "recharts";
import getPaginatedData from "./getPaginatedData";
import { Spinner, Container, Col, Row } from "react-bootstrap";
import "./TravelBarGraph.css";

const COLORS = [
  "#0088FE",
  "#00C49F",
  "#FFBB28",
  "#FF8042",
  "#440BD4",
  "#FF2079",
];

const groupData = (groups, currentValue, currentIndex) => {
  const f = currentValue.temp_average * (9 / 5) + 32;
  const group = Math.floor(f / 15);
  groups[group] = groups[group] ?? { value: 0 };
  groups[group].value += currentValue.tourists;
  return groups;
};

const parseData = (data) => {
  const res = data.reduce(groupData, {});
  const parsedData = Object.entries(res).map(([key, value], index) => {
    const intKey = parseInt(key);
    const lowerbound = intKey * 15;
    const upperbound = (intKey + 1) * 15 - 1;
    return {
      name: `${lowerbound} - ${upperbound}`,
      value: value["value"],
      fill: COLORS[index],
    };
  });
  return parsedData;
};

const tickFormatter = (value: string, index: number) => {
  const intVal = parseInt(value);
  if (intVal < 1000) return value;
  return `${intVal / 1000}k`;
};

const CustomTooltip = ({ active, payload, label }) => {
  if (active) {
    const tourists = payload[0].value.toLocaleString("en-US");
    return (
      <div className="custom-tooltip">
        <p className="intro">{tourists} tourists</p>
      </div>
    );
  }
  return null;
};

const TravelBarGraph = () => {
  const [data, setData] = useState(null);

  useEffect(() => {
    const api = "https://www.api.plansafetrips.me/countries?page_size=20";
    getPaginatedData(api).then((res) => {
      setData(res);
    });
  }, []);

  if (data === null) {
    return (
      <Container>
        <Row className="justify-content-center">
          <Spinner className="p-4 m-5" animation="border" />
        </Row>
      </Container>
    );
  }

  const parsedData = parseData(data);

  return (
    <Container className="data-control-bar">
      <h3>Tourists by Average Country Temperature (˚F)</h3>
      <BarChart width={600} height={400} data={parsedData} isAnimationActive>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis tickFormatter={tickFormatter} />
        <Tooltip content={CustomTooltip} />
        <Bar dataKey="value" fill="#8884d8" />
      </BarChart>
    </Container>
  );
};

export default TravelBarGraph;
