import React, { useEffect, useState, useRef } from "react";
import BubbleChart from "@weknow/react-bubble-chart-d3";
import getPaginatedData from "./getPaginatedData";
import { Container, Spinner, Tab, Row, Col } from "react-bootstrap";

const PopularAirports = () => {
  const [data, setData] = useState(null);
  const [selected, setSelected] = useState("");
  const airports = useRef({}); // airport code to total flights
  const show = 50;

  useEffect(() => {
    const api = "https://www.api.plansafetrips.me/flights?page_size=100";
    getPaginatedData(api).then((res) => {
      setData(res);

      // Populate airports
      airports.current = {};
      res.forEach((flight) => {
        flight.segments.forEach((segment) => {
          if (airports.current[segment.arrival_iata]) {
            airports.current[segment.arrival_iata].arrivals += 1;
          } else {
            airports.current[segment.arrival_iata] = {
              name: segment.arrival_name,
              arrivals: 1,
              departures: 0,
            };
          }
          if (airports.current[segment.departure_iata]) {
            airports.current[segment.departure_iata].departures += 1;
          } else {
            airports.current[segment.departure_iata] = {
              name: segment.departure_name,
              arrivals: 0,
              departures: 1,
            };
          }
        });
      });

      //Select most popular airport
      setSelected(
        Object.entries(airports.current).sort((a, b) => {
          return a[1].arrivals + a[1].departures >
            b[1].arrivals + b[1].departures
            ? -1
            : 1;
        })[0][0]
      );
    });
  }, []);

  if (data === null) {
    return (
      <Container>
        <Row className="justify-content-center">
          <Spinner className="p-4 m-5" animation="border" />
        </Row>
      </Container>
    );
  }

  return (
    <Container className="data-control-bar">
      <h3>Top {show} Busiest Airports</h3>
      <Row>
        <Col lg={8} xl={7}>
          <div className="bubble-chart">
            <BubbleChart
              graph={{ zoom: 0.7 }}
              padding={1}
              height={700}
              data={Object.entries(airports.current)
                .sort((a, b) => {
                  return a[1].arrivals + a[1].departures >
                    b[1].arrivals + b[1].departures
                    ? -1
                    : 1;
                })
                .slice(0, show)
                .map(([iata, airport]) => ({
                  label: iata,
                  value: airport.arrivals + airport.departures,
                }))}
              showLegend={false}
              valueFont={{
                family: "Arial",
                size: 12,
                color: "#fff",
                weight: "bold",
              }}
              labelFont={{
                family: "Arial",
                size: 14,
                color: "#fff",
              }}
              bubbleClickFun={(iata) => {
                setSelected(iata);
              }}
            />
          </div>
        </Col>
        <Col lg={4} xl={5} className="p-5">
          {selected !== "" && (
            <>
              <h2>{airports.current[selected].name}</h2>
              <p className="bubble-chart-subtitle">
                {airports.current[selected].arrivals} arrivals
              </p>
              <p className="bubble-chart-subtitle">
                {airports.current[selected].departures} departures
              </p>
            </>
          )}
        </Col>
      </Row>
    </Container>
  );
};

export default PopularAirports;
