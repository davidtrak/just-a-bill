import * as React from "react";
import { useState, useEffect } from "react";
import { pdfjs } from "react-pdf";
import pdfjsWorker from "pdfjs-dist/build/pdf.worker.entry";
import { fetchSingleData } from "Library/Data.js";

import axios from "axios";
import PDFViewer from "pdf-viewer-reactjs";

import "./BillInstance.css";

import { Container, Row, Col, Spinner } from "react-bootstrap";
import CongressCard from "Common/ModelCards/CongressCard/CongressCard";
import StateCard from "Common/ModelCards/StateCard/StateCard";
import { useParams } from "react-router-dom";
import useInstanceData from "Common/Hooks/useInstanceData";
import { Link } from "react-router-dom";

type Props = {};

const BillInstance = (props: Props): React.Node => {
  let { id } = useParams();

  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);
  const [pdf, setPdf] = useState(null);

  // const data = useInstanceData("bills", id);

  const [data, isLoading, isError] = useInstanceData("bills", id);

  // const [data, setData] = useState(null);
  const [stateData, setStateData] = useState(null);
  const [congressData, setCongressData] = useState(null);

  useEffect(() => {
    const load = async () => {
      console.log(data.sponsorStateId);
      console.log(data.sponsorId);

      let dState = await fetchSingleData("states", data.sponsorStateId);
      setStateData(dState);
      setCongressData(await fetchSingleData("congresspeople", data.sponsorId));
    };
    if (!isLoading) {
      load();
    }
  }, [data, isLoading]);

  useEffect(() => {
    const api = "https://api.justabill.info/pdf/" + id;
    console.log("Getting from " + api);
    axios.get(api).then((response) => {
      console.log(response.data);
      setPdf(response.data);
    });
  }, [id]);

  // Data is null until done loading
  if (isLoading) {
    return (
      <Container>
        <Spinner className="p-4 m-5" animation="border" />
      </Container>
    );
  }

  pdfjs.GlobalWorkerOptions.workerSrc = pdfjsWorker;

  let onDocumentLoadSuccess = (pdfDocProxy) => {
    setNumPages(pdfDocProxy._pdfInfo.numPages);
  };

  let handleNextPage = () => {
    if (pageNumber + 1 <= numPages) {
      setPageNumber(pageNumber + 1);
    }
  };

  let handlePrevPage = () => {
    if (pageNumber - 1 >= 1) {
      setPageNumber(pageNumber - 1);
    }
  };

  const backgroundImg =
    "url(https://cdn.britannica.com/66/164166-050-4FBB1C5A/Chamber-US-House-of-Representatives-Washington-DC.jpg)";

  return (
    <React.Fragment>
      <div
        className="bill-inst"
        style={{
          backgroundImage: backgroundImg,
        }}
      >
        <Container>
          <Row className="bill-name">
            {data.number}{" "}
            {data.title === data.shortTitle ? "" : " : " + data.shortTitle}
          </Row>
          <Row className="bill-desc">{data.title}</Row>
          <Row className="bill-body">
            <Col className="bill-stats" xs={3}>
              <Row className="bill-geninfo">
                <h3>General Information</h3>
              </Row>
              <p>Date Introduced: {data.introducedDate}</p>
              <p>
                Proposed By:
                <Link
                  to={"/congresspeople/" + data.sponsorId}
                  className="cong-link"
                >
                  {" " + data.sponsorTitle} {data.sponsor}
                </Link>
              </p>
              <p>Proposing Party: {data.sponsorParty}</p>
              <p>Committees: {data.committees}</p>
              <p>Primary Subject: {data.primarySubject}</p>
              <p>Status: {data.active ? "Active" : "Inactive"}</p>
              <p>House Passage: {data.housePassage ? "Yes" : "No"}</p>
              <p>Senate Passage: {data.senatePassage ? "Yes" : "No"}</p>
            </Col>
            <Col className="bill-pdf">
              <Row>
                <h3>PDF of Proposed Bill</h3>
              </Row>
              {data.pdfUrl !== null ? (
                pdf !== null ? (
                  <PDFViewer document={{ base64: pdf }} />
                ) : (
                  ""
                )
              ) : (
                <p>No PDF exists for this bill...</p>
              )}
            </Col>
          </Row>
          <Row className="mt-3">
            <Col className="bill-state-card">
              <h3>Congressperson State</h3>
              {stateData ? (
                <StateCard data={stateData} />
              ) : (
                <Spinner className="p-4 m-5" animation="border" />
              )}
            </Col>
          </Row>
          <Row className="mt-3">
            <Col className="bill-cong-card">
              <h3>Introducer</h3>
              {congressData ? (
                <CongressCard data={congressData} />
              ) : (
                <Spinner className="p-4 m-5" animation="border" />
              )}
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
};
export default BillInstance;
