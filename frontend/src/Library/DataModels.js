// @flow

import axios from "axios";
import { createClass } from "react";
import congressWikiUrls from "Library/CongressWikiUrls";

export type DataModel = CongresspersonModel | StateModel | BillModel;

interface Model {
  [fieldName: string]: string | boolean | number;
}

export class CongresspersonModel implements Model {
  age: number;
  numBillsCosponsored: number;
  numBillsSponsored: number;
  chamber: string;
  committees: string;
  contactForm: string;
  dateOfBirth: string;
  district: ?string;
  facebookAccount: string;
  fax: ?string;
  firstName: string;
  fullName: string;
  gender: string;
  headshotUrl: string;
  id: number;
  inOffice: boolean;
  lastName: string;
  lastUpdated: string;
  leadershipRole: ?string;
  bioguideId: string;
  middleName: ?string;
  missedVotes: number;
  missedVotesPct: number;
  mostRecentVote: string;
  nextElection: string;
  numCommittees: number;
  office: string;
  party: string;
  phone: string;
  senateClass: string;
  seniority: number;
  shortTitle: string;
  state: string;
  stateId: number;
  stateAbbreviation: string;
  stateRank: string;
  suffix: ?string;
  termsServed: number;
  title: string;
  totalVotes: number;
  twitterAccount: string;
  website: string;
  votesAgainstPartyPct: number;
  votesWithPartyPct: number;
  youtubeAccount: string;
  wikipediaUrl: string;
  description: ?string;
  constructor(data) {
    this.id = data.id;
    this.bioguideId = data.memberId;
    this.fullName = data.full_name;
    this.firstName = data.first_name;
    this.middleName = data.middle_name;
    this.lastName = data.last_name;
    this.suffix = data.suffix;
    this.title = data.title;
    this.shortTitle = data.short_title;
    this.headshotUrl = data.headshot_url;
    this.age = data.age;
    this.dateOfBirth = data.date_of_birth;
    this.gender = data.gender === "M" ? "Male" : "Female";
    this.party = {
      D: "Democrat",
      R: "Republican",
      ID: "Independent",
    }[data.party];
    this.chamber = data.chamber;
    this.seniority = data.seniority;
    this.termsServed = data.terms_served;
    this.leadershipRole =
      data.leadership_role === "" ? null : data.leadership_role;
    this.committees = data.committees;
    this.numCommittees = data.num_committees;
    this.numBillsSponsored = data.bills_sponsored;
    this.numBillsCosponsored = data.bills_cosponsored;
    this.inOffice = data.in_office;
    this.nextElection = data.next_election;
    if (this.chamber === "Senate") this.senateClass = data.senate_class;
    this.wikipediaUrl =
      congressWikiUrls[this.fullName.replace(/[A-Z]\.\s/g, "")];

    this.stateId = data.state_id;
    this.state = data.state_name;
    this.stateAbbreviation = data.stateAbbreviation;
    this.stateRank = data.state_rank;
    if (this.chamber === "House") this.district = data.district;

    this.totalVotes = data.total_votes;
    this.missedVotes = data.missed_votes;
    this.missedVotesPct = data.missed_votes_pct;
    this.votesWithPartyPct = data.votes_with_party_pct;
    this.votesAgainstPartyPct = data.votes_against_party_pct;
    this.mostRecentVote = data.most_recent_vote;

    this.phone = data.phone;
    this.office = data.office;
    this.fax = data.fax;
    this.contactForm = data.contact_form;
    this.website = data.url;
    this.twitterAccount = data.twitter_account;
    this.facebookAccount = data.facebook_account;
    this.youtubeAccount = data.youtube_account;

    this.lastUpdated = data.last_updated;
  }
}

export class StateModel implements Model {
  abbreviation: string;
  capital: string;
  description: string;
  established: string;
  ethnicGroups: string;
  flagUrl: string;
  id: number;
  inPoverty: number;
  largestCity: string;
  location: string;
  medianHouseholdIncome: number;
  name: string;
  numRepresentatives: number;
  numProposedBills: number;
  party: string;
  population: number;
  size: number;
  constructor(data) {
    this.abbreviation = data.abbreviation;
    this.capital = data.capital;
    this.description = data.description;
    this.established = data.established;
    this.ethnicGroups = data.ethnic_groups;
    this.flagUrl = data.flag;
    this.id = data.id;
    this.inPoverty = data.in_poverty;
    this.largestCity = data.largest_city;
    this.location = data.location;
    this.medianHouseholdIncome = data.median_household_income;
    this.name = data.name;
    this.numRepresentatives = data.num_congressmen;
    this.numProposedBills = data.num_proposed_bills;
    this.party = data.party_affiliation;
    this.population = data.population;
    this.size = data.size;
  }
}

export class BillModel implements Model {
  active: boolean;
  number: string;
  slug: string;
  billType: string;
  committees: string;
  cosponsors: number;
  enacted: ?boolean;
  housePassage: ?boolean;
  id: number;
  introducedDate: string;
  lastVoteDate: ?string;
  latestMajorAction: string;
  latestMajorActionDate: string;
  pdfUrl: string;
  primarySubject: string;
  senatePassage: ?boolean;
  shortTitle: string;
  sponsor: string;
  sponsorId: string;
  sponsorParty: string;
  sponsorState: string;
  sponsorTitle: string;
  sponsorStateId: number;
  summary: string;
  summaryShort: string;
  title: string;
  vetoed: ?boolean;
  constructor(data) {
    this.active = data.active;
    this.number = data.bill;
    this.slug = data.bill_slug;
    this.billType = data.bill_type;
    this.committees = data.committees;
    this.cosponsors = data.cosponsors;
    this.enacted = data.enacted;
    this.housePassage = data.house_passage;
    this.id = data.id;
    this.introducedDate = data.introduced_date;
    this.lastVoteDate = data.last_vote;
    this.latestMajorAction = data.latest_major_action;
    this.latestMajorActionDate = data.latest_major_action_date;
    this.pdfUrl = data.pdf_url;
    this.primarySubject = data.primary_subject;
    this.senatePassage = data.senate_passage;
    this.shortTitle = data.short_title;
    this.sponsor = data.sponsor;
    this.sponsorId = data.congressperson_id;
    this.sponsorParty = {
      D: "Democrat",
      R: "Republican",
      ID: "Independent",
    }[data.sponsor_party];
    this.sponsorState = data.sponsor_state;
    this.sponsorStateId = data.state_id;
    this.sponsorTitle = data.sponsor_title;
    this.summary = data.summary;
    this.summaryShort = data.summary_short;
    this.title = data.title;
    this.vetoed = data.vetoed;
  }
}
