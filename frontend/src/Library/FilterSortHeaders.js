// @flow

export type FilterHeader = {
  [filterTitle: string]: {
    queryParam: string,

    // Mapping from option title to query parameter value, or
    // if the query parameter value is the same as the title then
    // just a list of titles
    options: { [optionTitle: string]: string } | Array<string>,
  },
};

// Mapping from option titles to query parameter value
export type SortHeader = {
  [displayTitle: string]: string,
};

const states = {
  Alabama: "AL",
  Alaska: "AK",
  Arizona: "AZ",
  Arkansas: "AR",
  California: "CA",
  Colorado: "CO",
  Connecticut: "CT",
  Delaware: "DE",
  Florida: "FL",
  Georgia: "GA",
  Hawaii: "HI",
  Idaho: "ID",
  Illinois: "IL",
  Indiana: "IN",
  Iowa: "IA",
  Kansas: "KS",
  Kentucky: "KY",
  Louisiana: "LA",
  Maine: "ME",
  Maryland: "MD",
  Massachusetts: "MA",
  Michigan: "MI",
  Minnesota: "MN",
  Mississippi: "MS",
  Missouri: "MO",
  Montana: "MT",
  Nebraska: "NE",
  Nevada: "NV",
  "New Hampshire": "NH",
  "New Jersey": "NJ",
  "New Mexico": "NM",
  "New York": "NY",
  "North Carolina": "NC",
  "North Dakota": "ND",
  Ohio: "OH",
  Oklahoma: "OK",
  Oregon: "OR",
  Pennsylvania: "PA",
  "Rhode Island": "RI",
  "South Carolina": "SC",
  "South Dakota": "SD",
  Tennessee: "TN",
  Texas: "TX",
  Utah: "UT",
  Vermont: "VT",
  Virginia: "VA",
  Washington: "WA",
  "West Virginia": "WV",
  Wisconsin: "WI",
  Wyoming: "WY",
};

const parties = {
  Republican: "R",
  Democrat: "D",
  Independent: "ID",
};

export const congressFilterHeader: FilterHeader = {
  Party: {
    queryParam: "party",
    options: parties,
  },
  Chamber: {
    queryParam: "short_title",
    options: {
      Senate: "Sen.",
      House: "Rep.",
    },
  },
  State: {
    queryParam: "state",
    options: states,
  },
  Gender: {
    queryParam: "gender",
    options: { Male: "M", Female: "F" },
  },
  Role: {
    queryParam: "leadership_role",
    options: [
      "Senate Democratic Caucus Secretary",
      "Senate Republican Conference Chair",
      "Senate Majority Whip",
      "Senate Minority Whip",
      "Senate Republican Conference Vice Chair",
      "President Pro Tempore of the Senate",
      "Senate Democratic Steering Committee Chair",
      "Senate Minority Leader",
      "Assistant Senate Minority Leader",
      "Senate Majority Leader",
      "Senate Democratic Policy & Communications Committee Chair",
      "Speaker of the House",
      "Assistant Speaker of the House",
      "House Majority Whip",
      "House Minority Whip",
      "House Democratic Caucus Vice Chair",
      "House Republican Conference Chair",
      "House Majority Leader",
      "House Republican Conference Secretary",
      "House Democratic Caucus Chair",
      "House Republican Conference Vice Chair",
      "House Minority Leader",
      "House Republican Policy Committee Chair",
    ],
  },
};

export const congressSortHeader: SortHeader = {
  Name: "full_name",
  Age: "age",
  "Next Election": "next_election",
  Seniority: "seniority",
  "Terms Served": "terms_served",
  "Bills Sponsored": "bills_sponsored",
};

export const stateFilterHeader: FilterHeader = {
  Region: {
    queryParam: "location",
    options: ["Northeast", "Midwest", "South", "West"],
  },
  "Majority Party": {
    queryParam: "party_affiliation",
    options: {
      Republican: "Republican",
      Democrat: "Democrat",
      Mixed: "Even",
    },
  },
  Size: {
    queryParam: "size",
    options: {
      "Small (<25k sq. miles)": "small",
      "Medium (25-70k sq. miles)": "medium",
      "Large (>70k sq. miles)": "large",
    },
  },
  Population: {
    queryParam: "population",
    options: {
      "Small (<1 mil)": "small",
      "Medium (1-10 mil)": "medium",
      "Large (>10 mil)": "large",
    },
  },
};

export const stateSortHeader: SortHeader = {
  Name: "name",
  Size: "size",
  Population: "population",
  "Number of representatives": "num_congressmen",
  "Number of bills proposed": "num_proposed_bills",
  "Median household income": "median_household_income",
  "People in poverty": "in_poverty",
  "Established date": "established",
};

export const billFilterHeader: FilterHeader = {
  Origin: {
    queryParam: "sponsor_title",
    options: {
      Senate: "Sen.",
      House: "Rep.",
    },
  },
  Status: {
    queryParam: "active",
    options: { Active: "True", Inactive: "False" },
  },
  "Sponsor's Party": {
    queryParam: "sponsor_party",
    options: parties,
  },
  "Sponsor's State": {
    queryParam: "sponsor_state",
    options: states,
  },
};

export const billSortHeader: SortHeader = {
  Title: "short_title",
  "Bill Number": "bill",
  "Date Introduced": "introduced_date",
  "Latest Major Action Date": "latest_major_action_date",
  "Number of cosponsors": "cosponsors",
};
