// @flow

import axios from "axios";

import mockCongressList from "Assets/MockData/CongressList";
import mockStatesList from "Assets/MockData/StatesList";
import mockBillsList from "Assets/MockData/BillsList";
import mockCongressDict from "Assets/MockData/CongressFull";
import mockStatesDict from "Assets/MockData/StatesFull";
import mockBillsDict from "Assets/MockData/BillsFull";
import { CongresspersonModel, StateModel, BillModel } from "Library/DataModels";
import type { DataModel } from "Library/DataModels";

const API_URL = "https://api.justabill.info";

export type Endpoint =
  | "congresspeople"
  | "states"
  | "bills"
  | "mock:congresspeople"
  | "mock:states"
  | "mock:bills";

// Returns a dictionary with the requested data page
export async function fetchListData(
  source: Endpoint,
  currentPage?: number = 1,
  resultsPerPage?: number = 20,
  filterString?: string = "",
  sortBy?: string = "",
  searchFor?: string = ""
): Promise<{ totalResults: number, page: Array<DataModel> }> {
  if (source === "mock:congresspeople")
    return fetchMockListData(mockCongressList, currentPage, resultsPerPage);
  if (source === "mock:states")
    return fetchMockListData(mockStatesList, currentPage, resultsPerPage);
  if (source === "mock:bills")
    return fetchMockListData(mockBillsList, currentPage, resultsPerPage);

  // Construct query
  let query = `${API_URL}/${source}?`;
  query += `page=${currentPage}&per_page=${resultsPerPage}`;
  if (filterString !== "") query += `&${filterString}`;
  if (sortBy !== "") query += `&sort=${sortBy}`;
  if (searchFor !== "") query += `&search=${searchFor}`;

  // Fetch result
  console.log(`Fetching from ${query}`);
  let result = await axios.get(query);

  let ret = { totalResults: result.data.total_results, page: [] };
  result.data.page.forEach((obj) => {
    ret.page.push(processData(source, obj));
  });
  return ret;
}

// Returns a dictionary with the requested data page
export async function fetchSingleData(
  source: string,
  id: string
): Promise<DataModel> {
  if (source === "mock:congresspeople")
    return fetchMockSingleData(mockCongressDict, id);
  if (source === "mock:states") return fetchMockSingleData(mockStatesDict, id);
  if (source === "mock:bills") return fetchMockSingleData(mockBillsDict, id);

  console.log(`Fetching from ${API_URL}/${source}/${id}`);
  let result = await axios.get(`${API_URL}/${source}/${id}`);
  return processData(source, result.data);
}

// Parses the mock data dictionaries based on specified criteria
async function fetchMockListData(
  list: any[],
  currentPage: number,
  resultsPerPage: number,
  filterBy?: string,
  sortBy?: string,
  searchFor?: string,
  mockDelay?: number = 800
): Promise<any> {
  console.log("Fetching mock data");

  let results = [];

  for (
    let i = (currentPage - 1) * resultsPerPage;
    i < Math.min(currentPage * resultsPerPage, list.length);
    i++
  ) {
    results.push(list[i]);
  }

  // Wait for some amount of milliseconds to simulate a real API call
  await new Promise((r) => setTimeout(r, mockDelay));

  return {
    status: "Ok",
    total_results: list.length,
    page: results,
  };
}

async function fetchMockSingleData(
  dict: { ... },
  id: string,
  mockDelay?: number = 800
): Promise<any> {
  // Wait for some amount of milliseconds to simulate a real API call
  await new Promise((r) => setTimeout(r, mockDelay));

  return dict[id];
}

const processData = (
  source: Endpoint,
  data: { [string]: mixed }
): DataModel => {
  if (source.includes("congress")) {
    return new CongresspersonModel(data);
  } else if (source.includes("state")) {
    return new StateModel(data);
  } else if (source.includes("bill")) {
    return new BillModel(data);
  } else {
    console.log("Error processing data. Couldn't read source");
    return new CongresspersonModel({});
  }
};
