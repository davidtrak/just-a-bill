from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from time import sleep

HOST_NAME = "https://develop.d2mqx3pxykbmdn.amplifyapp.com/"


class TestGUI:
    def test_app(self, browser):
        browser.get(HOST_NAME)
        app = browser.find_element(by=By.CSS_SELECTOR, value=".app-div")
        assert app != None

    # Test NavBar Links
    def test_about_link(self, browser):
        browser.get(HOST_NAME)
        about_link = browser.find_element(By.LINK_TEXT, value="About")
        about_link.click()
        sleep(3)
        browser.refresh()
        about_page = browser.find_element(By.CSS_SELECTOR, value=".aboutInfo")
        about_title = browser.find_element(By.TAG_NAME, value="h1")
        assert about_page != None
        assert about_title.text == "About Us"

    def test_states_link(self, browser):
        browser.get(HOST_NAME)
        states_link = browser.find_element(By.LINK_TEXT, value="States")
        states_link.click()
        sleep(3)
        browser.refresh()
        states_page = browser.find_element(By.CSS_SELECTOR, value=".model-page-title")
        states_title = browser.find_element(By.TAG_NAME, value="p")
        assert states_page != None
        assert states_title.text == "States"

    def test_bills_link(self, browser):
        browser.get(HOST_NAME)
        bills_link = browser.find_element(By.LINK_TEXT, value="Bills")
        bills_link.click()
        sleep(3)
        browser.refresh()
        bills_page = browser.find_element(By.CSS_SELECTOR, value=".model-page-title")
        bills_title = browser.find_element(By.TAG_NAME, value="p")
        assert bills_page != None
        assert bills_title.text == "Bills"

    def test_congresspeople_link(self, browser):
        browser.get(HOST_NAME)
        congress_link = browser.find_element(By.LINK_TEXT, value="Congresspeople")
        congress_link.click()
        sleep(3)
        browser.refresh()
        congress_page = browser.find_element(By.CSS_SELECTOR, value=".model-page-title")
        congress_title = browser.find_element(By.TAG_NAME, value="p")
        assert congress_page != None
        assert congress_title.text == "Congresspeople"

    def test_home_link(self, browser):
        browser.get(HOST_NAME)
        home_link = browser.find_element(By.LINK_TEXT, value="justabill")
        home_link.click()
        sleep(3)
        browser.refresh()
        home_page = browser.find_element(By.CSS_SELECTOR, value=".title")
        home_title = browser.find_element(By.TAG_NAME, value="div")
        assert home_page != None
        assert "justabill" in home_title.text

    # Test Model Pages have Cards
    def test_about_page(self, browser):
        browser.get(HOST_NAME + "about")
        about_member_title = browser.find_elements(By.TAG_NAME, value="h1")
        about_cards = browser.find_elements(By.CSS_SELECTOR, value=".card-body")
        assert "Team Members" == about_member_title[1].text
        assert "Rigo Garcia" in about_cards[0].text

    # TODO: may have to edit this once we pull data from out api, store actual string?
    def test_states_page(self, browser):
        browser.get(HOST_NAME + "states")
        state_cards = browser.find_elements(
            By.CSS_SELECTOR, value=".state-card.card.border-dark"
        )
        assert len(state_cards) >= 3

        card_name = browser.find_elements(
            By.CSS_SELECTOR, value=".state-card-title.card-title.h5"
        )
        card_state_name = card_name[0].text
        state_cards[0].click()
        sleep(3)
        model_state_name = browser.find_elements(By.CSS_SELECTOR, value=".state-title")
        assert card_state_name == model_state_name[0].text  # test card matches model

    def test_bills_page(self, browser):
        browser.get(HOST_NAME + "bills")
        bill_cards = browser.find_elements(By.CSS_SELECTOR, value=".display-table-row")
        assert len(bill_cards) >= 3

        bill_names = browser.find_elements(By.TAG_NAME, value="td")
        td_bill_name = bill_names[0].text
        td_bill_title = bill_names[1].text
        bill_cards[0].click()
        sleep(3)
        model_bill_name = browser.find_elements(By.CSS_SELECTOR, value=".bill-name")
        bill_title = td_bill_name + " : " + td_bill_title
        assert bill_title == model_bill_name[0].text  # test card matches model

    def test_congresspeople_page(self, browser):
        browser.get(HOST_NAME + "congresspeople")
        congress_cards = browser.find_elements(
            By.CSS_SELECTOR, value=".cong-card.card.border-dark"
        )
        assert len(congress_cards) >= 3

        card_name = browser.find_elements(
            By.CSS_SELECTOR, value=".cong-card-title.card-title.h5"
        )
        card_congress_name = card_name[0].text
        congress_cards[0].click()
        sleep(3)
        model_congress_name = browser.find_elements(By.CSS_SELECTOR, value=".cong-name")
        assert (
            card_congress_name in model_congress_name[0].text
        )  # test card matches model

    # Test Instance Pages have data.. TODO: will have to edit the ids to be real ids
    def test_states_instance(self, browser):
        browser.get(HOST_NAME + "states/0")
        state_title = browser.find_elements(By.CSS_SELECTOR, value=".state-title")
        state_desc = browser.find_elements(By.TAG_NAME, value="p")
        state_congresspeople_card = browser.find_elements(
            By.CSS_SELECTOR, value=".state-congbill-card"
        )
        print(state_congresspeople_card[0].text)
        assert state_title != None
        assert state_desc != None
        assert "Congress Members" in state_congresspeople_card[0].text

    def test_bills_instance(self, browser):
        browser.get(HOST_NAME + "bills/0")
        bill_title = browser.find_elements(By.CSS_SELECTOR, value=".bill-name")
        bill_desc = browser.find_elements(By.CSS_SELECTOR, value=".bill-desc")
        bill_info = browser.find_element(By.CSS_SELECTOR, value=".bill-stats")
        assert bill_title != None
        assert bill_desc != None
        assert "Date Introduced:" in bill_info.text
        assert "Proposed By:" in bill_info.text
        assert "House Passage:" in bill_info.text

    def test_congress_instance(self, browser):
        browser.get(HOST_NAME + "congresspeople/0")
        congress_name = browser.find_elements(By.CSS_SELECTOR, value=".cong-name")
        congress_desc = browser.find_elements(By.TAG_NAME, value="p")
        congress_info = browser.find_elements(By.CSS_SELECTOR, value=".cong-box")
        assert congress_name != None
        assert congress_desc != None
        assert "Seniority" in congress_info[0].text
        assert "Age" in congress_info[0].text

    def test_filter_states(self, browser):
        browser.get(HOST_NAME + "states")
        states_header = browser.find_elements(
            By.CSS_SELECTOR, value=".model-page-title"
        )
        assert states_header[0].text == "States"
        sleep(3)
        # check california there
        california_card = browser.find_elements(
            By.CSS_SELECTOR, value=".state-card-title.card-title.h5"
        )
        assert "California" in california_card[4].text
        # click the filter button
        selections = browser.find_elements(By.CSS_SELECTOR, value=".filters")
        selections[0].click()
        sleep(1)
        button = browser.find_elements(By.CSS_SELECTOR, value=".form-check-inline")
        button[0].click()
        sleep(3)
        # check that element 4 is no longer california (not republican)
        florida_card = browser.find_elements(
            By.CSS_SELECTOR, value=".state-card-title.card-title.h5"
        )
        assert "Florida" in florida_card[4].text

    def test_search_states(self, browser):
        browser.get(HOST_NAME + "states")
        search_bar = browser.find_elements(By.CSS_SELECTOR, value=".form-control")[0]
        # search_bar[0].click()
        search_bar.send_keys("California")
        search_bar.send_keys(Keys.ENTER)
        sleep(5)
        california_card = browser.find_elements(
            By.CSS_SELECTOR, value=".state-card-title.card-title.h5"
        )
        assert "California" in california_card[0].text

    def test_states_sort(self, browser):
        browser.get(HOST_NAME + "states")
        states_header = browser.find_elements(
            By.CSS_SELECTOR, value=".model-page-title"
        )
        assert states_header[0].text == "States"
        sleep(3)
        # check california there
        california_card = browser.find_elements(
            By.CSS_SELECTOR, value=".state-card-title.card-title.h5"
        )
        assert "California" in california_card[4].text
        # click the sort button (defaults to sorting by name alphabetically, so we'll flip it)
        selections = browser.find_elements(By.CSS_SELECTOR, value=".pill-button-wrapper")[5]
        selections.click()
        sleep(3)
        # check that Wyoming is the first state
        wyoming_card = browser.find_elements(
            By.CSS_SELECTOR, value=".state-card-title.card-title.h5"
        )
        assert "Wyoming" in wyoming_card[0].text
